#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "utilsMesh.h"

#include <QVector>
#include <vector>

using namespace std;

class Dijkstra {
public:
  Dijkstra();

  const vector<int>& get_paths();
  const vector<int>& get_currentPath();

  void dijkstra(MyMesh* _mesh, int _vertexStart);
  const vector<int>& calc_path(MyMesh* _mesh, int vertexEnd);

private:
  vector<int> allPaths;
  vector<int> currentPath;
  vector<int> tabWeights;
  int vertexStart;

  bool tous_sommets_extraits(const QVector<bool>& tab);
  int extraire_min(const QVector<bool>& listeSommets);
};

#endif // DIJKSTRA_H
