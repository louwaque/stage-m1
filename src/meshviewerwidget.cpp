#include "meshviewerwidget.h"

void tearVao(QOpenGLVertexArrayObject** vao) {
  if (*vao) {
    if ((*vao)->isCreated()) {
      (*vao)->destroy();
    }
    delete (*vao);
    (*vao) = nullptr;
  }
}

MeshViewerWidget::Draw& MeshViewerWidget::getOrNewDraw(DrawId& id, const Draw& defaultDraw) {
  if (id == 0) {
    id = next_id++;
  }
  auto it = m_draws.find(id);
  if (it != m_draws.end()) {
    return it->second;
  } else {
    m_draws.insert({id, defaultDraw});
    return m_draws[id];
  }
}

void MeshViewerWidget::clear() {
  makeCurrent();
  for (auto p : m_draws) {
    p.second.tearGLObjects();
  }
  m_draws.clear();
  doneCurrent();
}

void MeshViewerWidget::Draw::tearTriGLObjects() {
  triToDraw = 0;
  tearVao(&triVao);
  for (QOpenGLBuffer& buff : TriDataBuffers) {
    if (buff.isCreated()) {
      buff.destroy();
    }
  }
}

void MeshViewerWidget::Draw::tearLinesGLObjects() {
  linesToDraw = 0;
  tearVao(&linesVao);
  for (QOpenGLBuffer& buff : TriDataBuffers) {
    if (buff.isCreated()) {
      buff.destroy();
    }
  }
  edgeSizes.clear();
}

void MeshViewerWidget::Draw::tearPointsGLObjects() {
  pointsToDraw = 0;
  tearVao(&pointsVao);
  for (QOpenGLBuffer& buff : PointsDataBuffers) {
    if (buff.isCreated()) {
      buff.destroy();
    }
  }
  vertsSizes.clear();
}

void MeshViewerWidget::Draw::tearGLObjects() {
  tearTriGLObjects();
  tearLinesGLObjects();
  tearPointsGLObjects();
}

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";

MeshViewerWidget::MeshViewerWidget(QWidget* _parent) : QOpenGLWidget(_parent) {
  QSurfaceFormat sf;
  sf.setDepthBufferSize(24);
  sf.setSamples(4);
  if (OPENGL_DEBUG) {
    sf.setOption(QSurfaceFormat::DebugContext);
  }
  setFormat(sf);

  setEnabled(true);
  setFocusPolicy(Qt::StrongFocus);
  setFocus();
}

MeshViewerWidget::~MeshViewerWidget() {
  clear();
}

QMatrix4x4& MeshViewerWidget::matrix(DrawId id) {
  return m_draws.at(id).matrix;
}

const QMatrix4x4& MeshViewerWidget::matrix(DrawId id) const {
  return m_draws.at(id).matrix;
}

bool& MeshViewerWidget::isShown(DrawId id) {
  return m_draws.at(id).isShown;
}

bool MeshViewerWidget::isShown(DrawId id) const {
  return m_draws.at(id).isShown;
}

float& MeshViewerWidget::alphaThreshold(DrawId id) {
  return m_draws.at(id).alphaThreshold;
}

float MeshViewerWidget::alphaThreshold(DrawId id) const {
  return m_draws.at(id).alphaThreshold;
}

void MeshViewerWidget::remove(DrawId id) {
  makeCurrent();
  m_draws.at(id).tearGLObjects();
  m_draws.erase(id);
  doneCurrent();
}

void MeshViewerWidget::messageLogged(QOpenGLDebugMessage message) {
  qDebug() << message;
}

void MeshViewerWidget::initializeGL() {
  initializeOpenGLFunctions();

  if (OPENGL_DEBUG) {
    QOpenGLDebugLogger* m_debugLogger = new QOpenGLDebugLogger(context());
    if (m_debugLogger->initialize()) {
      qDebug() << "GL_DEBUG Debug Logger" << m_debugLogger << "\n";
      connect(m_debugLogger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(messageLogged(QOpenGLDebugMessage)));
      m_debugLogger->startLogging();
    }
  }

  glClearColor(1.0, 1.0, 1.0, 1.0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_MULTISAMPLE);

  m_modelView.setToIdentity();
  set_scene_pos(QVector3D(0.0, 0.0, 0.0), 1.0);

  m_program = new QOpenGLShaderProgram(this);
  m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);
  m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);

  if (!m_program->link()) {
    qWarning("Failed to compile and link shader program:");
    qWarning() << m_program->log();
  }
}

void MeshViewerWidget::translate(const QVector3D& _trans) {
  QMatrix4x4 translation;
  translation.translate(_trans);
  m_modelView = translation * m_modelView;
}

void MeshViewerWidget::resizeGL(int _w, int _h) {
  update_projection_matrix();
  glViewport(0, 0, _w, _h);
  update();
}

void MeshViewerWidget::loadMesh(Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* triangles, int nTriangles) {
  QVector<GLfloat> vertsColsArray;
  vertsColsArray.reserve(nVerts * (4 + 3));

  for (int i = 0, c = 0, v = 0; i < nVerts;) {
    vertsColsArray << colors[c] / 255.0 << colors[c + 1] / 255.0 << colors[c + 2] / 255.0 << colors[c + 3] / 255.0 << verts[v]
                   << verts[v + 1] << verts[v + 2];
    ++i;
    c = i * 4;
    v = i * 3;
  }

  auto vao = new QOpenGLVertexArrayObject;
  vao->create();
  vao->bind();

  QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
  vboArray.create();
  vboArray.bind();
  vboArray.allocate(vertsColsArray.constData(), vertsColsArray.count() * sizeof(GLfloat));

  QOpenGLBuffer vboElements(QOpenGLBuffer::IndexBuffer);
  vboElements.create();
  vboElements.bind();
  vboElements.allocate(triangles, nTriangles * sizeof(GLuint));

  m_program->setAttributeBuffer("colAttr", GL_FLOAT, 0, 4, 7 * sizeof(GLfloat));
  m_program->setAttributeBuffer("posAttr", GL_FLOAT, 4 * sizeof(GLfloat), 3, 7 * sizeof(GLfloat));

  m_program->enableAttributeArray("colAttr");
  m_program->enableAttributeArray("posAttr");

  vao->release();

  draw.tearTriGLObjects();
  draw.TriDataBuffers[0] = vboArray;
  draw.TriDataBuffers[1] = vboElements;
  draw.triToDraw         = nTriangles;
  draw.triVao            = vao;
}

void MeshViewerWidget::loadLines(
    Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* lines, int nLines, QList<QPair<float, int>> es) {
  QVector<GLfloat> linesColsArray;
  linesColsArray.reserve(nVerts * (4 + 3));

  for (int i = 0, c = 0, v = 0; i < nVerts;) {
    linesColsArray << colors[c] / 255.0 << colors[c + 1] / 255.0 << colors[c + 2] / 255.0 << colors[c + 3] / 255.0 << verts[v]
                   << verts[v + 1] << verts[v + 2];
    ++i;
    c = i * 4;
    v = i * 3;
  }

  auto vao = new QOpenGLVertexArrayObject;
  vao->create();
  vao->bind();

  QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
  vboArray.create();
  vboArray.bind();
  vboArray.allocate(linesColsArray.constData(), linesColsArray.count() * sizeof(GLfloat));

  QOpenGLBuffer vboElements(QOpenGLBuffer::IndexBuffer);
  vboElements.create();
  vboElements.bind();
  vboElements.allocate(lines, nLines * sizeof(GLuint));

  m_program->setAttributeBuffer("colAttr", GL_FLOAT, 0, 4, 7 * sizeof(GLfloat));
  m_program->setAttributeBuffer("posAttr", GL_FLOAT, 4 * sizeof(GLfloat), 3, 7 * sizeof(GLfloat));

  m_program->enableAttributeArray("colAttr");
  m_program->enableAttributeArray("posAttr");

  vao->release();

  draw.tearLinesGLObjects();
  draw.LinesDataBuffers[0] = vboArray;
  draw.LinesDataBuffers[1] = vboElements;
  draw.linesToDraw         = nLines;
  draw.edgeSizes           = es;
  draw.linesVao            = vao;
}

void MeshViewerWidget::loadPoints(
    Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* points, int nPoints, QList<QPair<float, int>> vs) {
  QVector<GLfloat> pointsColsArray;
  pointsColsArray.reserve(nVerts * (4 + 3));

  for (int i = 0, c = 0, v = 0; i < nVerts;) {
    pointsColsArray << colors[c] / 255.0 << colors[c + 1] / 255.0 << colors[c + 2] / 255.0 << colors[c + 3] / 255.0 << verts[v]
                    << verts[v + 1] << verts[v + 2];
    ++i;
    c = i * 4;
    v = i * 3;
  }

  auto vao = new QOpenGLVertexArrayObject;
  vao->create();
  vao->bind();

  QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
  vboArray.create();
  vboArray.bind();
  vboArray.allocate(pointsColsArray.constData(), pointsColsArray.count() * sizeof(GLfloat));

  QOpenGLBuffer vboElements(QOpenGLBuffer::IndexBuffer);
  vboElements.create();
  vboElements.bind();
  vboElements.allocate(points, nPoints * sizeof(GLuint));

  m_program->setAttributeBuffer("colAttr", GL_FLOAT, 0, 4, 7 * sizeof(GLfloat));
  m_program->setAttributeBuffer("posAttr", GL_FLOAT, 4 * sizeof(GLfloat), 3, 7 * sizeof(GLfloat));

  m_program->enableAttributeArray("colAttr");
  m_program->enableAttributeArray("posAttr");

  vao->release();

  draw.tearPointsGLObjects();
  draw.PointsDataBuffers[0] = vboArray;
  draw.PointsDataBuffers[1] = vboElements;
  draw.pointsToDraw         = nPoints;
  draw.vertsSizes           = vs;
  draw.pointsVao            = vao;
}

// charge un objet MyMesh dans l'environnement OpenGL
DrawId MeshViewerWidget::displayMesh(Mesh* _mesh, DisplayMode mode, DrawId id) {
  makeCurrent();

  static Draw defaultDraw;
  Draw& draw = getOrNewDraw(id, defaultDraw);
  draw.tearGLObjects();

  GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
  GLfloat* triCols       = new GLfloat[_mesh->n_faces() * 3 * 4];
  GLfloat* triVerts      = new GLfloat[_mesh->n_faces() * 3 * 3];

  int i = 0;

  if (mode == DisplayMode::TemperatureMap) {
    QVector<float> values;
    for (Mesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
      values.append(fabs(_mesh->data(*curVert).value));
    std::sort(values.begin(), values.end());

    float range = values.at(values.size() * 0.8);

    Mesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
    Mesh::ConstFaceVertexIter fvIt;

    for (; fIt != fEnd; ++fIt) {
      fvIt               = _mesh->cfv_iter(*fIt);
      triCols[4 * i + 3] = _mesh->data(*fvIt).alpha;
      if (_mesh->data(*fvIt).value > 0) {
        triCols[4 * i + 0] = 255;
        triCols[4 * i + 1] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 2] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      } else {
        triCols[4 * i + 2] = 255;
        triCols[4 * i + 1] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 0] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      }
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 3] = _mesh->data(*fvIt).alpha;
      if (_mesh->data(*fvIt).value > 0) {
        triCols[4 * i + 0] = 255;
        triCols[4 * i + 1] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 2] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      } else {
        triCols[4 * i + 2] = 255;
        triCols[4 * i + 1] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 0] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      }
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 3] = _mesh->data(*fvIt).alpha;
      if (_mesh->data(*fvIt).value > 0) {
        triCols[4 * i + 0] = 255;
        triCols[4 * i + 1] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 2] = 255 - std::min((_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      } else {
        triCols[4 * i + 2] = 255;
        triCols[4 * i + 1] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
        triCols[4 * i + 0] = 255 - std::min((-_mesh->data(*fvIt).value / range) * 255.0, 255.0);
      }
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
    }
  }

  if (mode == DisplayMode::Normal) {
    Mesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
    Mesh::ConstFaceVertexIter fvIt;
    for (; fIt != fEnd; ++fIt) {
      fvIt                = _mesh->cfv_iter(*fIt);
      triCols[4 * i + 0]  = _mesh->color(*fIt)[0];
      triCols[4 * i + 1]  = _mesh->color(*fIt)[1];
      triCols[4 * i + 2]  = _mesh->color(*fIt)[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 0]  = _mesh->color(*fIt)[0];
      triCols[4 * i + 1]  = _mesh->color(*fIt)[1];
      triCols[4 * i + 2]  = _mesh->color(*fIt)[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 0]  = _mesh->color(*fIt)[0];
      triCols[4 * i + 1]  = _mesh->color(*fIt)[1];
      triCols[4 * i + 2]  = _mesh->color(*fIt)[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
    }
  }

  if (mode == DisplayMode::ColorShading) {
    Mesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
    Mesh::ConstFaceVertexIter fvIt;
    for (; fIt != fEnd; ++fIt) {
      fvIt                = _mesh->cfv_iter(*fIt);
      triCols[4 * i + 0]  = _mesh->data(*fvIt).faceShadingColor[0];
      triCols[4 * i + 1]  = _mesh->data(*fvIt).faceShadingColor[1];
      triCols[4 * i + 2]  = _mesh->data(*fvIt).faceShadingColor[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 0]  = _mesh->data(*fvIt).faceShadingColor[0];
      triCols[4 * i + 1]  = _mesh->data(*fvIt).faceShadingColor[1];
      triCols[4 * i + 2]  = _mesh->data(*fvIt).faceShadingColor[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
      ++fvIt;
      triCols[4 * i + 0]  = _mesh->data(*fvIt).faceShadingColor[0];
      triCols[4 * i + 1]  = _mesh->data(*fvIt).faceShadingColor[1];
      triCols[4 * i + 2]  = _mesh->data(*fvIt).faceShadingColor[2];
      triCols[4 * i + 3]  = _mesh->data(*fvIt).alpha;
      triVerts[3 * i + 0] = _mesh->point(*fvIt)[0];
      triVerts[3 * i + 1] = _mesh->point(*fvIt)[1];
      triVerts[3 * i + 2] = _mesh->point(*fvIt)[2];
      triIndiceArray[i]   = i;

      i++;
    }
  }

  loadMesh(draw, triVerts, triCols, _mesh->n_faces() * 3, triIndiceArray, _mesh->n_faces() * 3);

  delete[] triIndiceArray;
  delete[] triCols;
  delete[] triVerts;

  GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
  GLfloat* linesCols       = new GLfloat[_mesh->n_edges() * 2 * 4];
  GLfloat* linesVerts      = new GLfloat[_mesh->n_edges() * 2 * 3];

  i = 0;
  QHash<float, QList<int>> edgesIDbyThickness;
  for (Mesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit) {
    float t = _mesh->data(*eit).thickness;
    if (t > 0) {
      if (!edgesIDbyThickness.contains(t))
        edgesIDbyThickness[t] = QList<int>();
      edgesIDbyThickness[t].append((*eit).idx());
    }
  }
  QHashIterator<float, QList<int>> it(edgesIDbyThickness);
  QList<QPair<float, int>> edgeSizes;
  while (it.hasNext()) {
    it.next();

    for (int e = 0; e < it.value().size(); e++) {
      int eidx = it.value().at(e);

      Mesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
      linesVerts[3 * i + 0]  = _mesh->point(vh1)[0];
      linesVerts[3 * i + 1]  = _mesh->point(vh1)[1];
      linesVerts[3 * i + 2]  = _mesh->point(vh1)[2];
      linesCols[4 * i + 0]   = _mesh->color(_mesh->edge_handle(eidx))[0];
      linesCols[4 * i + 1]   = _mesh->color(_mesh->edge_handle(eidx))[1];
      linesCols[4 * i + 2]   = _mesh->color(_mesh->edge_handle(eidx))[2];
      linesCols[4 * i + 3]   = _mesh->data(vh1).alpha;
      linesIndiceArray[i]    = i;
      i++;

      Mesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
      linesVerts[3 * i + 0]  = _mesh->point(vh2)[0];
      linesVerts[3 * i + 1]  = _mesh->point(vh2)[1];
      linesVerts[3 * i + 2]  = _mesh->point(vh2)[2];
      linesCols[4 * i + 0]   = _mesh->color(_mesh->edge_handle(eidx))[0];
      linesCols[4 * i + 1]   = _mesh->color(_mesh->edge_handle(eidx))[1];
      linesCols[4 * i + 2]   = _mesh->color(_mesh->edge_handle(eidx))[2];
      linesCols[4 * i + 3]   = _mesh->data(vh2).alpha;
      linesIndiceArray[i]    = i;
      i++;
    }
    edgeSizes.append(qMakePair(it.key(), it.value().size()));
  }

  loadLines(draw, linesVerts, linesCols, i, linesIndiceArray, i, edgeSizes);

  delete[] linesIndiceArray;
  delete[] linesCols;
  delete[] linesVerts;

  GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
  GLfloat* pointsCols       = new GLfloat[_mesh->n_vertices() * 4];
  GLfloat* pointsVerts      = new GLfloat[_mesh->n_vertices() * 3];

  i = 0;
  QHash<float, QList<int>> vertsIDbyThickness;
  for (Mesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit) {
    float t = _mesh->data(*vit).thickness;
    if (t > 0) {
      if (!vertsIDbyThickness.contains(t))
        vertsIDbyThickness[t] = QList<int>();
      vertsIDbyThickness[t].append((*vit).idx());
    }
  }
  QHashIterator<float, QList<int>> vitt(vertsIDbyThickness);
  QList<QPair<float, int>> vertsSizes;

  while (vitt.hasNext()) {
    vitt.next();

    for (int v = 0; v < vitt.value().size(); v++) {
      int vidx = vitt.value().at(v);

      pointsVerts[3 * i + 0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
      pointsVerts[3 * i + 1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
      pointsVerts[3 * i + 2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
      pointsCols[4 * i + 0]  = _mesh->color(_mesh->vertex_handle(vidx))[0];
      pointsCols[4 * i + 1]  = _mesh->color(_mesh->vertex_handle(vidx))[1];
      pointsCols[4 * i + 2]  = _mesh->color(_mesh->vertex_handle(vidx))[2];
      pointsCols[4 * i + 3]  = _mesh->data(_mesh->vertex_handle(vidx)).alpha;
      pointsIndiceArray[i]   = i;
      i++;
    }
    vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
  }

  loadPoints(draw, pointsVerts, pointsCols, i, pointsIndiceArray, i, vertsSizes);

  delete[] pointsIndiceArray;
  delete[] pointsCols;
  delete[] pointsVerts;

  doneCurrent();

  return id;
}

void MeshViewerWidget::paintGL() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  m_program->bind();

  QMatrix4x4 m = m_projection * m_modelView;

  for (auto it : m_draws) {
    auto& draw = it.second;
    if (!draw.isShown) {
      continue;
    }

    auto matrix = m * draw.matrix;
    m_program->setUniformValue("matrix", matrix);
    m_program->setUniformValue("alphaThreshold", draw.alphaThreshold);

    if (draw.triToDraw > 0) {
      glPolygonOffset(1.0, 2);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

      glEnable(GL_POLYGON_OFFSET_FILL);
      draw.triVao->bind();
      glDrawElements(GL_TRIANGLES, draw.triToDraw, GL_UNSIGNED_INT, 0);
      draw.triVao->release();
      glDisable(GL_POLYGON_OFFSET_FILL);
    }

    if (draw.linesToDraw > 0) {
      draw.linesVao->bind();
      int cur = 0;
      for (int i = 0; i < draw.edgeSizes.count(); i++) {
        glLineWidth(draw.edgeSizes.at(i).first);
        glDrawElements(GL_LINES, draw.edgeSizes.at(i).second * 2, GL_UNSIGNED_INT, (GLvoid*)(sizeof(GLfloat) * cur));
        cur = cur + draw.edgeSizes.at(i).second * 2;
      }
      draw.linesVao->release();
    }

    if (draw.pointsToDraw > 0) {
      draw.pointsVao->bind();
      int cur = 0;
      for (int i = 0; i < draw.vertsSizes.count(); i++) {
        glPointSize(draw.vertsSizes.at(i).first);
        glDrawElements(GL_POINTS, draw.vertsSizes.at(i).second, GL_UNSIGNED_INT, (GLvoid*)(sizeof(GLfloat) * cur));
        cur = cur + draw.vertsSizes.at(i).second;
      }
      draw.pointsVao->release();
    }
  }

  m_program->release();
}

void MeshViewerWidget::set_scene_pos(const QVector3D& _cog, float _radius) {
  m_center = _cog;
  m_radius = _radius;
  update_projection_matrix();
  view_all();
}

void MeshViewerWidget::update_projection_matrix() {
  m_projection.setToIdentity();
  m_projection.perspective(45.f, static_cast<float>(width()) / static_cast<float>(height()), 0.01f * m_radius, 100.f * m_radius);
}

void MeshViewerWidget::view_all() {
  QVector3D translation = m_modelView * m_center;
  translation[2] += 3.f * m_radius;
  translation *= -1;
  translate(translation);
}

void MeshViewerWidget::mousePressEvent(QMouseEvent* _event) {
  last_point_ok_ = map_to_sphere(last_point_2D_ = _event->pos(), last_point_3D_);
}

void MeshViewerWidget::mouseMoveEvent(QMouseEvent* _event) {
  float* modelview_matrix_ = m_modelView.data();

  QPoint newPoint2D = _event->pos();

  QVector3D newPoint3D;
  bool newPoint_hitSphere = map_to_sphere(newPoint2D, newPoint3D);

  float dx = newPoint2D.x() - last_point_2D_.x();
  float dy = newPoint2D.y() - last_point_2D_.y();

  float w = width();
  float h = height();

  if ((_event->buttons() == (Qt::LeftButton + Qt::MidButton)) ||
      (_event->buttons() == Qt::LeftButton && _event->modifiers() == Qt::ControlModifier)) {
    float value_y = m_radius * dy * 3.f / h;
    translate(QVector3D(0.f, 0.f, value_y));
  } else if ((_event->buttons() == Qt::MidButton) ||
             (_event->buttons() == Qt::LeftButton && _event->modifiers() == Qt::AltModifier)) {
    float z = -(modelview_matrix_[2] * m_center[0] + modelview_matrix_[6] * m_center[1] + modelview_matrix_[10] * m_center[2] +
                modelview_matrix_[14]) /
              (modelview_matrix_[3] * m_center[0] + modelview_matrix_[7] * m_center[1] + modelview_matrix_[11] * m_center[2] +
               modelview_matrix_[15]);
    float aspect     = w / h;
    float near_plane = 0.01f * m_radius;
    float top        = std::tan(45.f / 2.f * M_PI / 180.f) * near_plane;
    float right      = aspect * top;
    translate(QVector3D(2.f * dx / w * right / near_plane * z, -2.f * dy / h * top / near_plane * z, 0.0f));
  } else if (_event->buttons() == Qt::LeftButton) {
    if (last_point_ok_) {
      if ((newPoint_hitSphere = map_to_sphere(newPoint2D, newPoint3D))) {
        QVector3D axis = QVector3D::crossProduct(last_point_3D_, newPoint3D);
        if (axis.lengthSquared() < 1e-7f)
          axis = QVector3D(1, 0, 0);
        else
          axis.normalize();
        QVector3D d = last_point_3D_ - newPoint3D;
        float t     = 0.5f * d.length() / TRACKBALL_RADIUS;
        if (t < -1.f)
          t = -1.f;
        else if (t > 1.f)
          t = 1.0;
        float phi   = 2.f * std::asin(t);
        float angle = phi * 180.f / M_PI;
        rotate(axis, angle);
      }
    }
  }
  last_point_2D_ = newPoint2D;
  last_point_3D_ = newPoint3D;
  last_point_ok_ = newPoint_hitSphere;
  update();
}

void MeshViewerWidget::mouseReleaseEvent(QMouseEvent* /* _event */) {
  last_point_ok_ = false;
}

void MeshViewerWidget::wheelEvent(QWheelEvent* _event) {
  float d = -static_cast<float>(_event->delta()) / 120.f * 0.2f * m_radius;
  translate(QVector3D(0.0, 0.0, d));
  update();
  _event->accept();
}

bool MeshViewerWidget::map_to_sphere(const QPoint& _point, QVector3D& _result) {
  float x    = (2.f * _point.x() - width()) / width();
  float y    = -(2.f * _point.y() - height()) / height();
  float xval = x;
  float yval = y;
  float x2y2 = xval * xval + yval * yval;

  const float rsqr = TRACKBALL_RADIUS * TRACKBALL_RADIUS;
  _result[0]       = xval;
  _result[1]       = yval;
  if (x2y2 < 0.5f * rsqr)
    _result[2] = std::sqrt(rsqr - x2y2);
  else
    _result[2] = 0.5f * rsqr / std::sqrt(x2y2);

  return true;
}

void MeshViewerWidget::rotate(const QVector3D& _axis, float _angle) {
  QVector3D t = m_modelView * m_center;

  QMatrix4x4 rotation;
  rotation.setToIdentity();
  rotation.translate(t);
  rotation.rotate(_angle, _axis);
  rotation.translate(-t);

  m_modelView = rotation * m_modelView;
}
