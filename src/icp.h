#ifndef ICP_H
#define ICP_H

#include "mesh.h"

struct ICPResult {
  bool hasConverged;
  double fitnessScore;
  Matrix4x4f transformation;
};

// icp nature
ICPResult icp(const Mesh& sourceMesh, const Mesh& targetMesh, int maximumIterations);

// icp avec trie sur une propriété
ICPResult icp(const Mesh& sourceMesh,
              const Mesh& targetMesh,
              int maximumIterations,
              std::function<float(const Mesh&, Mesh::VertexHandle)> getProperty,
              float propertyThreshold);

#endif