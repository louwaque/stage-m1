#ifndef MESH_H
#define MESH_H

#include "types.h"

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

struct MeshTraits : public OpenMesh::DefaultTraits {
  typedef Vector3f Point;
  typedef Vector3f Normal;
  typedef float TexCoord1D;
  typedef Vector2f TexCoord2D;
  typedef Vector3f TexCoord3D;
  typedef int TextureIndex;
  typedef Vector3uc Color;

  VertexAttributes(OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color);
  HalfedgeAttributes(OpenMesh::Attributes::PrevHalfedge);
  FaceAttributes(OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color);
  EdgeAttributes(OpenMesh::Attributes::Color);
  VertexTraits {
    // Draw
    float thickness;
    float value;
    Color faceShadingColor;
    unsigned char alpha;

    // Algo
    float barycentricArea, kCurv, hCurv;
    bool isKeyPoint;
    Normal normalizedNormal;

    // Autres
    float distance_aux_faces;
  };
  EdgeTraits {
    // Draw
    float thickness;

    // Algo
    float dihedralAngle;
  };
  FaceTraits {
    // Algo
    float area;
    Normal normalizedNormal;
  };
};

class Mesh : public OpenMesh::TriMesh_ArrayKernelT<MeshTraits> {
public:
  using TriMesh_ArrayKernelT<MeshTraits>::TriMesh_ArrayKernelT;
  void resetAllColorsAndThickness();
  void faceVertices(FaceHandle face, Mesh::Point vertices[3]) const;
  void nRingVertices(VertexHandle vh, size_t n, std::vector<Mesh::VertexHandle>& ring) const;

  // met à jour la boîte englobante et keyPoints
  void updateProperties();

  const Point& boundingBoxMin() const { return _boundingBoxMin; }
  const Point& boundingBoxMax() const { return _boundingBoxMax; }

  const std::vector<VertexHandle>& keyPoints() const { return _keyPoints; }
  const std::vector<FaceHandle>& keyFaces() const { return _keyFaces; }
  const Normal& keyPointsNormal() const { return _keyPointsNormal; }

  // charge un maillage depuis un fichier
  // les sommets jaunes sont définis comme étant des keyPoint
  // retourne vrai si le chargement est ok
  bool chargement(const std::string& fileName);

private:
  Point _boundingBoxMin, _boundingBoxMax;
  std::vector<VertexHandle> _keyPoints;
  std::vector<FaceHandle> _keyFaces;
  Normal _keyPointsNormal;
};

// vérifie que le sommet donné appartient à l'arête et retourne l'autre sommet de l'arête
// retourne un VertexHandle invalide si le sommet n'appartient pas à l'arête
template<typename MeshT>
typename MeshT::VertexHandle oppositeVertex(const MeshT& mesh, typename MeshT::EdgeHandle eh, typename MeshT::VertexHandle vh) {
  auto vh0 = mesh.to_vertex_handle(mesh.halfedge_handle(eh, 0));
  auto vh1 = mesh.to_vertex_handle(mesh.halfedge_handle(eh, 1));

  if (vh == vh0) {
    return vh1;
  } else if (vh == vh1) {
    return vh0;
  } else {
    return typename MeshT::VertexHandle();
  }
}

// retourne le sommet commun aux deux arêtes
// retourne un VertexHandle invalide s'il n'y a pas de sommet commun
template<typename MeshT>
typename MeshT::VertexHandle commonVertex(const MeshT& mesh, typename MeshT::EdgeHandle eh0, typename MeshT::EdgeHandle eh1) {
  auto vh00 = mesh.to_vertex_handle(mesh.halfedge_handle(eh0, 0));
  auto vh01 = mesh.to_vertex_handle(mesh.halfedge_handle(eh0, 1));
  auto vh10 = mesh.to_vertex_handle(mesh.halfedge_handle(eh1, 0));
  auto vh11 = mesh.to_vertex_handle(mesh.halfedge_handle(eh1, 1));

  if (vh00 == vh10 || vh00 == vh11) {
    return vh00;
  } else if (vh01 == vh10 || vh01 == vh11) {
    return vh01;
  } else {
    return typename MeshT::VertexHandle();
  }
}

// converti un polygone d'écrit par des arêtes en polygone d'écrit par des sommets
template<typename MeshT>
std::vector<typename MeshT::VertexHandle> edgesToVertices(const MeshT& mesh,
                                                          const std::vector<typename MeshT::EdgeHandle>& edges) {
  std::vector<typename MeshT::VertexHandle> vertices;

  if (edges.size() == 1) {
    vertices.reserve(2);
    auto vh0 = mesh.to_vertex_handle(mesh.halfedge_handle(edges[0], 0));
    auto vh1 = mesh.to_vertex_handle(mesh.halfedge_handle(edges[0], 1));
    vertices.push_back(vh0);
    vertices.push_back(vh1);
  } else if (edges.size() > 1) {
    vertices.reserve(edges.size() + 1);
    vertices.push_back(oppositeVertex(mesh, edges[0], commonVertex(mesh, edges[0], edges[1])));

    for (size_t i = 0; i < edges.size(); ++i) {
      auto oppositeV = oppositeVertex(mesh, edges[i], vertices.back());
      assert(oppositeV.is_valid());
      vertices.push_back(oppositeV);
    }

    if (vertices.front() == vertices.back()) {
      vertices.pop_back();
    }
  }

  return vertices;
}

#endif
