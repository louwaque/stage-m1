#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "meshviewerwidget.h"
#include <QFileDialog>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

// les méthodes de cette classe ne sont pas destinnées à être lu
// car elles changent tout le temps, au gré des tests que je mène

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = 0);
  ~MainWindow();

private:
  void H_Curv(Mesh* _mesh);
  void K_Curv(Mesh* _mesh);
  bool chargement(Mesh* _mesh, QString& fileNameVar, const QString& fileName);
  bool chargement(Mesh* _mesh, QString& fileNameVar);

private slots:
  void on_pushButton_chargement_1_clicked();
  void on_pushButton_chargement_2_clicked();
  void on_checkBox_isShown_1_clicked();
  void on_checkBox_isShown_2_clicked();
  void on_checkBox_keyPts_1_clicked();
  void on_checkBox_keyPts_2_clicked();

  void on_pushButton_H_clicked();
  void on_pushButton_K_clicked();
  void on_distance_aux_faces_compute_clicked();
  void on_distance_aux_faces_move_clicked();
  void on_protruding_edges_compute_clicked();
  void on_protruding_edges_export_clicked();
  void on_pushButton_decimation_clicked();
  void on_correspondences_compute_clicked();
  void on_export_prop_compute_clicked();
  void on_acp_compute_clicked();

  void on_icp_compute_clicked();
  void on_icp_show_transform_clicked();
  void on_icp_apply_transform_clicked();

  void on_hausdorff_compute_clicked();

  void update_meshes();

private:
  QString filename1, filename2;
  Mesh mesh1, mesh2;
  DrawId mesh1drawID = 0, mesh2drawID = 0;
  bool mesh1Loaded = false, mesh2Loaded = false;
  Matrix4x4f lastIcpTransform = Matrix4x4f::Identity();

  Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
