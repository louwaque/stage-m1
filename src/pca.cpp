#include "pca.h"

#include <pcl/common/pca.h>
#include <pcl/point_types.h>

PCAResult pca(const Mesh& mesh) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;

  size_t nbKeyPoints = std::count_if(
      mesh.vertices_begin(), mesh.vertices_end(), [&mesh](Mesh::VertexHandle vh) { return mesh.data(vh).isKeyPoint; });
  cloud.reset(new pcl::PointCloud<pcl::PointXYZ>(nbKeyPoints, 1));
  size_t cloudIndex = 0;
  for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
    if (!mesh.data(*v_it).isKeyPoint) {
      continue;
    }
    cloud->at(cloudIndex).getVector3fMap() = mesh.point(*v_it);
    cloudIndex += 1;
  }

  pcl::PCA<pcl::PointXYZ> pca;
  pca.setInputCloud(cloud);

  PCAResult result;
  result.eigenvectors = std::move(pca.getEigenVectors());
  result.coefficients = std::move(pca.getCoefficients());
  result.mean         = std::move(pca.getMean());
  result.eigenvalues  = std::move(pca.getEigenValues());
  return result;
}