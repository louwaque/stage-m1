varying lowp vec4 col;
uniform float alphaThreshold;

void main() {
  if (col.a < alphaThreshold) {
    discard;
  }
  gl_FragColor = col;
}
