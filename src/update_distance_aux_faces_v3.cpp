#include "update_distance_aux_faces_v3.h"

#include "algo.h"
#include "meshviewerwidget.h"

extern MeshViewerWidget* MESH_VIEWER;

#include <iostream>

#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_triangle_primitive.h>
#include <CGAL/Simple_cartesian.h>

typedef CGAL::Simple_cartesian<float> K;
typedef K::FT FT;
typedef K::Ray_3 Ray;
typedef K::Line_3 Line;
typedef K::Point_3 Point;
typedef K::Vector_3 Vector;
typedef K::Direction_3 Direction;
typedef K::Triangle_3 Triangle;

// The following primitive provides the conversion facilities between
// the custom triangle and point types and the CGAL ones
struct My_triangle_primitive {
  class FaceIter : public Mesh::FaceIter {
  public:
    using Mesh::FaceIter::FaceIter;
    // FaceIter(const FaceIter& it) : Mesh::FaceIter(it) {}
    FaceIter(const Mesh::FaceIter& it) : Mesh::FaceIter(it) {}

    // FaceIter& operator=(const FaceIter& it) { return *this; }

    const Mesh* mesh() const { return dynamic_cast<const Mesh*>(mesh_); }
  };

public:
  // this is the type of data that the queries returns. For this example
  // we imagine that, for some reasons, we do not want to store the iterators
  // of the vector, but raw pointers. This is to show that the Id type
  // does not have to be the same as the one of the input parameter of the
  // constructor.
  typedef Mesh::FaceHandle Id;
  // CGAL types returned
  typedef K::Point_3 Point;    // CGAL 3D point type
  typedef K::Triangle_3 Datum; // CGAL 3D triangle type
private:
  Id m_pt; // this is what the AABB tree stores internally
  const Mesh* m_mesh = nullptr;

public:
  My_triangle_primitive() {} // default constructor needed
  // the following constructor is the one that receives the iterators from the
  // iterator range given as input to the AABB_tree
  My_triangle_primitive(Mesh::FaceIter it) : m_pt(*it), m_mesh(FaceIter(it).mesh()) {}
  My_triangle_primitive(Id pt, const Mesh* mesh) : m_pt(pt), m_mesh(mesh) {}

  const Id& id() const { return m_pt; }
  // utility function to convert a custom
  // point type to CGAL point type.
  Point convert(const Mesh::Point& p) const { return Point(p[0], p[1], p[2]); }
  // on the fly conversion from the internal data to the CGAL types
  Datum datum() const {
    Mesh::Point vertices[3];
    m_mesh->faceVertices(m_pt, vertices);
    return Datum(convert(vertices[0]), convert(vertices[1]), convert(vertices[2]));
  }
  // returns a reference point which must be on the primitive
  Point reference_point() const { return convert(m_mesh->point(m_mesh->to_vertex_handle(m_mesh->halfedge_handle(m_pt)))); }
};

typedef My_triangle_primitive Primitive;
typedef CGAL::AABB_traits<K, Primitive> AABB_triangle_traits;
typedef CGAL::AABB_tree<AABB_triangle_traits> Tree;
typedef boost::optional<Tree::Intersection_and_primitive_id<Ray>::Type> Ray_intersection;
typedef boost::optional<Tree::Intersection_and_primitive_id<Line>::Type> Line_intersection;

void update_distance_aux_faces_v3(Mesh& mesh1, Mesh& mesh2, bool onlyKeyFaces, bool useRay, float angleThreshold) {
  Mesh debug_mesh;

  auto update_distance_aux_faces = [=, &debug_mesh](Mesh& mesh1, const Mesh& mesh2) {
    Tree tree;
    if (onlyKeyFaces) {
      for (auto fh : mesh2.keyFaces()) {
        tree.insert(My_triangle_primitive(fh, &mesh2));
      }
    } else {
      tree.insert(mesh2.faces_begin(), mesh2.faces_end());
    }

    for (auto vh1 : mesh1.keyPoints()) {
      const auto& p                      = mesh1.point(vh1);
      const auto& n                      = mesh1.data(vh1).normalizedNormal;
      mesh1.data(vh1).distance_aux_faces = NAN;

      if (angle(n, mesh1.keyPointsNormal()) > angleThreshold) {
        continue;
      }

      if (useRay) {
        Ray ray_query(Point{p[0], p[1], p[2]}, Direction{n[0], n[1], n[2]});
        Ray_intersection intersection = tree.first_intersection(ray_query);

        if (intersection && boost::get<Point>(&(intersection->first))) {
          const Point* ip                    = boost::get<Point>(&(intersection->first));
          Vector3f v                         = Vector3f{ip->x(), ip->y(), ip->z()} - p;
          mesh1.data(vh1).distance_aux_faces = v.norm();

          auto vh1                       = debug_mesh.new_vertex(p);
          auto vh2                       = debug_mesh.new_vertex({ip->x(), ip->y(), ip->z()});
          auto eh                        = debug_mesh.edge_handle(debug_mesh.new_edge(vh1, vh2));
          debug_mesh.data(vh1).thickness = 12;
          debug_mesh.data(vh2).thickness = 12;
          debug_mesh.data(eh).thickness  = 10;

          debug_mesh.set_color(vh1, {255, 0, 255});
          debug_mesh.set_color(vh2, {0, 255, 255});
          debug_mesh.set_color(eh, {255, 255, 0});
        }
      } else {
        Line line_query(Point{p[0], p[1], p[2]}, Direction{n[0], n[1], n[2]});
        std::vector<Line_intersection> intersections;
        tree.all_intersections(line_query, std::back_inserter(intersections));

        float minSqrdDist = std::numeric_limits<float>::max();
        Vector3f minIp, minV;

        for (const auto& intersection : intersections) {
          if (boost::get<Point>(&(intersection->first))) {
            const Point* ip = boost::get<Point>(&(intersection->first));
            Vector3f v      = Vector3f{ip->x(), ip->y(), ip->z()} - p;
            float sqrdDist  = v.squaredNorm();
            if (sqrdDist < minSqrdDist) {
              minSqrdDist = sqrdDist;
              minIp       = Vector3f{ip->x(), ip->y(), ip->z()};
              minV        = v;
            }
          }
        }

        if (minSqrdDist != std::numeric_limits<float>::max()) {
          mesh1.data(vh1).distance_aux_faces = (minV.dot(n) >= 0.f ? 1.f : -1.f) * std::sqrt(minSqrdDist);

          auto vh1                       = debug_mesh.new_vertex(p);
          auto vh2                       = debug_mesh.new_vertex({minIp.x(), minIp.y(), minIp.z()});
          auto eh                        = debug_mesh.edge_handle(debug_mesh.new_edge(vh1, vh2));
          debug_mesh.data(vh1).thickness = 12;
          debug_mesh.data(vh2).thickness = 12;
          debug_mesh.data(eh).thickness  = 10;

          debug_mesh.set_color(vh1, {255, 0, 255});
          debug_mesh.set_color(vh2, {0, 255, 255});
          debug_mesh.set_color(eh, {255, 255, 0});
        }
      }
    }
  };

  update_distance_aux_faces(mesh1, mesh2);
  update_distance_aux_faces(mesh2, mesh1);

  static DrawId d = 0;
  d               = MESH_VIEWER->displayMesh(&debug_mesh, DisplayMode::Normal, d);
  MESH_VIEWER->update();
}