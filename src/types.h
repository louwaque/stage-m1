#ifndef TYPES_H
#define TYPES_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <OpenMesh/Core/Utils/color_cast.hh>
#include <OpenMesh/Core/Utils/vector_traits.hh>
#include <QMatrix4x4>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

using Vector2f   = Eigen::Matrix<float, 2, 1>;
using Vector3f   = Eigen::Matrix<float, 3, 1>;
using Vector3uc  = Eigen::Matrix<unsigned char, 3, 1>;
using Matrix4x4f = Eigen::Matrix<float, 4, 4>;
using Cloud      = pcl::PointCloud<pcl::PointXYZ>;

namespace OpenMesh {

#define EIGEN_VECTOR_TRAITS(VecType)                                                      \
  template<>                                                                              \
  struct vector_traits<VecType> {                                                         \
    typedef VecType vector_type;                                                          \
    typedef vector_type::Scalar value_type;                                               \
    static const size_t size_ = vector_type::RowsAtCompileTime;                           \
    static size_t size() { return size_; }                                                \
  };                                                                                      \
  inline VecType::Scalar dot(const VecType& v1, const VecType& v2) { return v1.dot(v2); } \
  inline VecType::Scalar norm(const VecType& v) { return v.norm(); }                      \
  inline VecType::Scalar sqrnorm(const VecType& v) { return v.squaredNorm(); }            \
  inline VecType& normalize(VecType& v) {                                                 \
    v.normalize();                                                                        \
    return v;                                                                             \
  }                                                                                       \
  template<typename OtherScalar>                                                          \
  inline VecType& vectorize(VecType& v, const OtherScalar& val) {                         \
    v.fill(val);                                                                          \
    return v;                                                                             \
  }

#define EIGEN_VECTOR3_TRAITS(VecType) \
  EIGEN_VECTOR_TRAITS(VecType)        \
  inline VecType cross(const VecType& v1, const VecType& v2) { return v1.cross(v2); }

EIGEN_VECTOR_TRAITS(Vector2f)
EIGEN_VECTOR3_TRAITS(Vector3f)
EIGEN_VECTOR3_TRAITS(Vector3uc)

template<>
struct color_caster<Vector3uc, OpenMesh::Vec3f> {
  typedef Vector3uc return_type;

  inline static return_type cast(const OpenMesh::Vec3f& _src) {
    return (Eigen::Array3f(_src.data()) * 255.f).cast<unsigned char>();
  }
};

} // namespace OpenMesh

inline void copy(QMatrix4x4& target, const Matrix4x4f& src) {
  for (size_t i = 0; i < 4; ++i) {
    for (size_t j = 0; j < 4; ++j) {
      target(i, j) = src(i, j);
    }
  }
}

#endif