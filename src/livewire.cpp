#include "livewire.h"

#include "algo.h"
#include "meshviewerwidget.h"
#include "pfe_livewire/contour.h"

#include <unordered_set>

extern MeshViewerWidget* MESH_VIEWER;

std::vector<Mesh::VertexHandle> get_protruding_vertices(Mesh& mesh, size_t edges_count, float edges_threshold) {
  float meanSqrEdgeLength = 0;
  for (auto e_it = mesh.edges_begin(); e_it != mesh.edges_end(); e_it++) {
    meanSqrEdgeLength += mesh.calc_edge_sqr_length(*e_it);
  }
  meanSqrEdgeLength /= mesh.n_edges();
  std::cout << "meanSqrEdgeLength = " << meanSqrEdgeLength << std::endl;

  float minSqrDistance = meanSqrEdgeLength * edges_threshold;
  std::cout << "minSqrDistance = " << minSqrDistance << std::endl;

  std::vector<Mesh::EdgeHandle> edges(mesh.edges_begin(), mesh.edges_end());
  std::sort(edges.begin(), edges.end(), [&mesh](Mesh::EdgeHandle eh1, Mesh::EdgeHandle eh2) {
    return mesh.data(eh1).dihedralAngle > mesh.data(eh2).dihedralAngle;
  });

  std::unordered_set<Mesh::VertexHandle> vertices_set;
  for (size_t i = 0; i < edges.size() && vertices_set.size() < edges_count; ++i) {
    auto heh0 = mesh.halfedge_handle(edges[i], 0);
    auto heh1 = mesh.halfedge_handle(edges[i], 1);
    auto vh0  = mesh.to_vertex_handle(heh0);
    auto vh1  = mesh.to_vertex_handle(heh1);

    if (!mesh.data(vh0).isKeyPoint || !mesh.data(vh1).isKeyPoint) {
      continue;
    }

    for (auto vh : {vh0, vh1}) {
      if (std::any_of(vertices_set.begin(), vertices_set.end(), [=](auto vh_set) {
            return (mesh.point(vh) - mesh.point(vh_set)).squaredNorm() < minSqrDistance;
          })) {
        continue;
      }

      vertices_set.insert(vh);
      mesh.data(vh).thickness = 15;
      mesh.set_color(vh, Mesh::Color(255, 0, 0));
    }
  }

  return {vertices_set.begin(), vertices_set.end()};
}

void sort_by_angle(Mesh& mesh, std::vector<Mesh::VertexHandle>& vertices) {
  struct VHandAngle {
    Mesh::VertexHandle vh;
    float angle;

    VHandAngle(Mesh::VertexHandle vh) : vh(vh) {}
  };

  std::vector<VHandAngle> vhangles(vertices.begin(), vertices.end());

  Vector3f meanPoint(0, 0, 0), meanNormal(0, 0, 0);
  for (const auto& v : vhangles) {
    meanPoint += mesh.point(v.vh);
    meanNormal += mesh.data(v.vh).normalizedNormal;
  }
  meanPoint /= vhangles.size();
  meanNormal.normalize();

  auto vectorMeanToProjected = [&](Mesh::VertexHandle vh) -> Vector3f {
    return projectPointToPlane(mesh.point(vh), meanPoint, meanNormal) - meanPoint;
  };

  Mesh m;

  auto base                 = vectorMeanToProjected(vhangles[0].vh);
  auto base_vh              = m.new_vertex(meanPoint);
  m.data(base_vh).thickness = 15;
  m.set_color(base_vh, Mesh::Color(0, 255, 0));

  for (auto& v : vhangles) {
    v.angle = signedAngle(base, vectorMeanToProjected(v.vh), meanNormal);
  }

  std::sort(
      vhangles.begin(), vhangles.end(), [&base](const VHandAngle& v1, const VHandAngle& v2) { return v1.angle < v2.angle; });

  for (size_t i = 0; i < vhangles.size(); ++i) {
    vertices[i] = vhangles[i].vh;

    auto vh              = m.new_vertex(projectPointToPlane(mesh.point(vertices[i]), meanPoint, meanNormal));
    m.data(vh).thickness = 15;
    float p              = i / float(vhangles.size() - 1) * 255;
    m.set_color(vh, Mesh::Color(p, p, p));
    auto eh              = m.edge_handle(m.new_edge(base_vh, vh));
    m.data(eh).thickness = 6;
    m.set_color(eh, Mesh::Color(0, 0, 255));
  }

  static DrawId d = 0;
  d               = MESH_VIEWER->displayMesh(&m, DisplayMode::Normal, d);
  MESH_VIEWER->update();
}

std::vector<MyMesh::EdgeHandle> LIVEWIRE_PATH;

std::vector<Mesh::VertexHandle> livewire(Mesh& mesh, const std::vector<Mesh::VertexHandle>& points) {
  MyMesh pfe_mesh;

  pfe_mesh.reserve(mesh.n_vertices(), mesh.n_edges(), mesh.n_faces());
  for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
    auto p  = mesh.point(*v_it);
    auto vh = pfe_mesh.new_vertex({p[0], p[1], p[2]});
  }

  std::vector<Mesh::VertexHandle> vertices;
  for (auto f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it) {
    vertices.clear();
    for (auto fv_it = mesh.cfv_ccwiter(*f_it); fv_it.is_valid(); ++fv_it) {
      vertices.push_back(*fv_it);
    }
    pfe_mesh.add_face(vertices);
  }

  pfe_mesh.update_normals();

  Vector3f normal(0, 0, 0), mean(0, 0, 0);
  Contour myContour(pfe_mesh);
  for (auto vh : points) {
    myContour.add_vertex(vh.idx());
    normal += mesh.normal(vh);
    mean += mesh.point(vh);
  }
  normal.normalize();
  mean /= points.size();

  float max_sqr_distance = 0;
  for (auto vh : points) {
    max_sqr_distance = std::max(max_sqr_distance, (mesh.point(vh) - mean).squaredNorm());
  }

  Vector3f sightPoint = mean + normal * std::sqrt(max_sqr_distance);
  MyMesh::Point _sightPoint(sightPoint[0], sightPoint[1], sightPoint[2]);
  LIVEWIRE_PATH.clear();
  myContour.draw_contour(&pfe_mesh, _sightPoint);

  auto result = edgesToVertices<MyMesh>(pfe_mesh, LIVEWIRE_PATH);

  for (size_t i = 0; i < result.size(); ++i) {
    auto heh                = mesh.find_halfedge(result[i], result[(i + 1) % result.size()]);
    auto eh                 = mesh.edge_handle(heh);
    mesh.data(eh).thickness = 6;
    mesh.set_color(eh, Mesh::Color(0, 255, 0));
  }

  Mesh debug_mesh;
  auto debug_mesh_vh = debug_mesh.add_vertex(sightPoint);
  debug_mesh.set_color(debug_mesh_vh, Mesh::Color(255, 255, 0));
  debug_mesh.data(debug_mesh_vh).thickness = 10;

  static DrawId d2 = 0;
  d2               = MESH_VIEWER->displayMesh(&debug_mesh, DisplayMode::Normal, d2);
  MESH_VIEWER->update();

  return result;
}