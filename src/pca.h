#ifndef PCA_H
#define PCA_H

#include "mesh.h"

struct PCAResult {
  // un vecteur par colonne
  Eigen::Matrix3f eigenvectors;
  Eigen::MatrixXf coefficients;
  Eigen::Vector4f mean;

  // rangés par ordre décroissant
  Eigen::Vector3f eigenvalues;
};

// effectue l'analyse en composantes principales à l'aide de PCL
PCAResult pca(const Mesh& mesh);

#endif