// inspiration from https://github.com/PointCloudLibrary/pcl/blob/master/tools/compute_hausdorff.cpp commit ebd8b12

#include "hausdorff.h"

#include <pcl/search/kdtree.h>

float hausdorff(const Cloud& cloud1, const Cloud& cloud2) {
  auto max_dist = [](const Cloud& from, const Cloud& to) {
    pcl::search::KdTree<Cloud::PointType> tree;
    tree.setInputCloud(to.makeShared());
    float max_sqr_dist = -std::numeric_limits<float>::max();
    for (const auto& point : from.points) {
      std::vector<int> indices(1);
      std::vector<float> sqr_distances(1);

      tree.nearestKSearch(point, 1, indices, sqr_distances);
      if (sqr_distances[0] > max_sqr_dist)
        max_sqr_dist = sqr_distances[0];
    }
    return std::sqrt(max_sqr_dist);
  };

  return std::max(max_dist(cloud1, cloud2), max_dist(cloud2, cloud1));
}