#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "algo.h"
#include "hausdorff.h"
#include "icp.h"
#include "livewire.h"
#include "pca.h"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>
#include <unordered_set>

#include <QDebug>

MeshViewerWidget* MESH_VIEWER = nullptr;

Vector3f DIST_FACE_MOVE(0, 0, 0);

struct LivewireResult {
  std::vector<Mesh::VertexHandle> points, result, exterior_points, exterior_result, interior_points, interior_result;
};

LivewireResult LIVEWIRE_M1, LIVEWIRE_M2;

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  MESH_VIEWER = ui->displayWidget;

  std::cout << std::showpoint << std::fixed << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
  show();

  // mesh1Loaded = chargement(
  // &mesh1, filename1, "/home/loic/Code/stage-m1/meshes/gargoyle/gargoyleHead-plain-selectedYellow_translatedHR.obj");
  // mesh2Loaded = chargement(&mesh2, filename2,
  // "/home/loic/Code/stage-m1/meshes/gargoyle/gargoyleBody-plain-selectedYellow.obj");

  // mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::Normal, mesh1drawID);
  // mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::Normal, mesh2drawID);
  // update_meshes();

  // on_pushButton_decimation_clicked();
  // on_icp_compute_clicked();
  // on_icp_apply_transform_clicked();
  // on_distance_aux_faces_compute_clicked();
  // on_protruding_edges_compute_clicked();
  // on_correspondences_compute_clicked();
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::H_Curv(Mesh* _mesh) {
  for (auto v = _mesh->vertices_begin(); v != _mesh->vertices_end(); ++v) {
    _mesh->data(*v).value = _mesh->data(*v).hCurv;
  }
}

void MainWindow::K_Curv(Mesh* _mesh) {
  for (auto v = _mesh->vertices_begin(); v != _mesh->vertices_end(); ++v) {
    _mesh->data(*v).value = _mesh->data(*v).kCurv;
  }
}

bool MainWindow::chargement(Mesh* _mesh, QString& fileNameVar, const QString& fileName) {
  fileNameVar = QFileInfo(fileName).fileName();
  return _mesh->chargement(fileName.toStdString());
}

bool MainWindow::chargement(Mesh* _mesh, QString& fileNameVar) {
  return chargement(_mesh, fileNameVar, QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)")));
}

void MainWindow::on_pushButton_chargement_1_clicked() {
  mesh1Loaded = chargement(&mesh1, filename1);

  mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::Normal, mesh1drawID);
  update_meshes();
}

void MainWindow::on_pushButton_chargement_2_clicked() {
  mesh2Loaded = chargement(&mesh2, filename2);

  mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::Normal, mesh2drawID);
  update_meshes();
}

void MainWindow::on_checkBox_isShown_1_clicked() {
  update_meshes();
}

void MainWindow::on_checkBox_isShown_2_clicked() {
  update_meshes();
}

void MainWindow::on_checkBox_keyPts_1_clicked() {
  update_meshes();
}

void MainWindow::on_checkBox_keyPts_2_clicked() {
  update_meshes();
}

void MainWindow::on_pushButton_H_clicked() {
  if (mesh1Loaded) {
    H_Curv(&mesh1);
    mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::TemperatureMap, mesh1drawID);
  }

  if (mesh2Loaded) {
    H_Curv(&mesh2);
    mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::TemperatureMap, mesh2drawID);
  }

  update_meshes();
}

void MainWindow::on_pushButton_K_clicked() {
  if (mesh1Loaded) {
    K_Curv(&mesh1);
    mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::TemperatureMap, mesh1drawID);
  }

  if (mesh2Loaded) {
    K_Curv(&mesh2);
    mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::TemperatureMap, mesh2drawID);
  }

  update_meshes();
}

void MainWindow::on_distance_aux_faces_compute_clicked() {
  Vector3f bBMin = mesh1.boundingBoxMin().cwiseMin(mesh2.boundingBoxMin()),
           bBMax = mesh1.boundingBoxMax().cwiseMax(mesh2.boundingBoxMax());
  float diag     = (bBMax - bBMin).norm();
  std::cout << "diag : " << diag << std::endl;

  auto update_colors = [](Mesh& mesh) {
    for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
      if (!mesh.data(*v_it).isKeyPoint) {
        mesh.data(*v_it).faceShadingColor = Mesh::Color(127, 127, 127);
        continue;
      }

      auto dist = mesh.data(*v_it).distance_aux_faces;
      Mesh::Color c;
      if (std::isnan(dist)) {
        c = Mesh::Color(127, 255, 127);
      } else if (dist >= 0) {
        c = Mesh::Color(255, 127, 127);
      } else {
        c = Mesh::Color(127, 127, 255);
      }
      mesh.data(*v_it).faceShadingColor = c;
    }
  };

  auto get_min_max_mean = [](const Mesh& mesh, float& min, float& max, float& mean) {
    min       = std::numeric_limits<float>::max();
    max       = std::numeric_limits<float>::min();
    float sum = 0.f;
    for (auto vh : mesh.keyPoints()) {
      float v = mesh.data(vh).distance_aux_faces;

      if (std::isnan(v)) {
        continue;
      }

      min = std::min(min, v);
      max = std::max(max, v);
      sum = sum + v;
    }
    mean = sum / mesh.keyPoints().size();
  };

  bool onlyKeyFaces              = ui->distance_aux_faces_keyPts->isChecked();
  bool useRay                    = false;
  QString distance_aux_faces_ray = ui->distance_aux_faces_ray->currentText();
  if (distance_aux_faces_ray == "Droite") {
    useRay = false;
  } else if (distance_aux_faces_ray == "Demi-droite") {
    useRay = true;
  }
  float angleThreshold = ui->distance_aux_faces_threshold->value() / 180.f * M_PI;

  // update_distance_aux_faces_v1(mesh1, mesh2);
  // update_distance_aux_faces_v2(mesh1, mesh2);
  update_distance_aux_faces_v3(mesh1, mesh2, onlyKeyFaces, useRay, angleThreshold);
  update_colors(mesh1);
  update_colors(mesh2);
  mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::ColorShading, mesh1drawID);
  mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::ColorShading, mesh2drawID);

  update_meshes();

  float m1min, m1max, m1mean, m2min, m2max, m2mean;
  get_min_max_mean(mesh1, m1min, m1max, m1mean);
  get_min_max_mean(mesh2, m2min, m2max, m2mean);
  ui->distance_aux_faces_results->setText(
      QString("Mesh 1:\nmin : %1, max : %2, moyenne : %3\nmin : %4%, max : %5%, moyenne : "
              "%6%\nMesh 2:\nmin : %7, max : %8, moyenne : %9\nmin : %10%, max : %11%, moyenne : %12%")
          .arg(m1min)
          .arg(m1max)
          .arg(m1mean)
          .arg(m1min / diag)
          .arg(m1max / diag)
          .arg(m1mean / diag)
          .arg(m2min)
          .arg(m2max)
          .arg(m2mean)
          .arg(m2min / diag)
          .arg(m2max / diag)
          .arg(m2mean / diag));

  std::cout << ui->distance_aux_faces_results->text().toStdString() << std::endl;

  Vector3f meanNormal = mesh1.keyPointsNormal();

  // on prend le vecteur propre avec la plus petite variance
  Vector3f pca_axe = pca(mesh1).eigenvectors.col(2);

  // on s'assure que l'axe principal va dans la direction opposé de la moyenne des normales pour pouvoir reculer
  DIST_FACE_MOVE = (angle(meanNormal, pca_axe) < M_PI_2 ? -pca_axe : pca_axe) * std::abs(std::min(m1min, m2min));
}

void MainWindow::on_distance_aux_faces_move_clicked() {
  for (auto v_it = mesh1.vertices_begin(); v_it != mesh1.vertices_end(); ++v_it) {
    mesh1.point(*v_it) += DIST_FACE_MOVE;
  }

  mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::Normal, mesh1drawID);
  update_meshes();
}

void MainWindow::on_protruding_edges_compute_clicked() {
  Mesh debug_mesh;

  auto show_protruding_edges = [this, &debug_mesh](Mesh& mesh) {
    mesh.resetAllColorsAndThickness();

    auto protruding_vertices =
        get_protruding_vertices(mesh, ui->protruding_edges_count->value(), ui->protruding_edges_threshold->value());
    sort_by_angle(mesh, protruding_vertices);

    auto livewire_path = livewire(mesh, protruding_vertices);

    std::vector<Mesh::VertexHandle> extended_path_interior, extended_path_exterior;
    std::unordered_set<Mesh::VertexHandle> set_protruding_vertices(protruding_vertices.begin(), protruding_vertices.end());

    for (size_t i = 0; i < livewire_path.size(); ++i) {
      auto vh = livewire_path[i];
      if (set_protruding_vertices.find(vh) == set_protruding_vertices.end()) {
        continue;
      }

      auto next_vh        = livewire_path[(i + 1) % livewire_path.size()];
      auto prev_vh        = livewire_path[i == 0 ? livewire_path.size() - 1 : i - 1];
      const auto& pt      = mesh.point(vh);
      const auto& next_pt = mesh.point(next_vh);
      const auto& prev_pt = mesh.point(prev_vh);

      mesh.set_color(next_vh, Mesh::Color(255, 85, 0));
      mesh.set_color(prev_vh, Mesh::Color(188, 62, 0));
      mesh.data(next_vh).thickness = 16;
      mesh.data(prev_vh).thickness = 16;

      std::vector<Mesh::VertexHandle> candidates;
      mesh.nRingVertices(vh, ui->protruding_edges_edge_ring->value(), candidates);

// livewire intérieur et extérieur
#if 0 // méthode loïc (corrigée)
      const auto& normal = mesh.data(vh).normalizedNormal;

      auto fill_extended_path =
          [&](const Vector3f& proj_prev_pt, const Vector3f& proj_next_pt, std::vector<Mesh::VertexHandle>& extended_path) {
            float aimed_angle = signedAngle(proj_prev_pt - pt, proj_next_pt - pt, normal);
            if (aimed_angle < 0.f) {
              aimed_angle += 2.f * M_PI;
            }
            aimed_angle /= 2.f;

            Eigen::AngleAxis<float> aa(aimed_angle, normal);
            Vector3f aimed_vector = aa * (proj_prev_pt - pt);

            auto d_vh1 = debug_mesh.new_vertex(pt);
            auto d_vh2 = debug_mesh.new_vertex(pt + aimed_vector.normalized() / 4.f);
            auto d_eh1 = debug_mesh.edge_handle(debug_mesh.new_edge(d_vh1, d_vh2));
            debug_mesh.set_color(d_eh1, Mesh::Color(200, 0, 0));
            debug_mesh.data(d_eh1).thickness = 10;

            float minDiff = std::numeric_limits<float>::max();
            Mesh::VertexHandle minVH;

            for (auto candidate : candidates) {
              if (candidate == prev_vh || candidate == next_vh) {
                continue;
              }

              const auto& current_pt = mesh.point(candidate);
              auto proj_current_pt   = projectPointToPlane(current_pt, pt, normal);

              float diff = angle(proj_current_pt - pt, aimed_vector);
              if (diff < minDiff) {
                minDiff = diff;
                minVH   = candidate;
              }
            }

            if (minVH.is_valid()) {
              extended_path.push_back(minVH);
              mesh.set_color(minVH, Mesh::Color(0, 85, 255));
              mesh.data(minVH).thickness = 16;
            }
          };

      auto proj_next_pt = projectPointToPlane(next_pt, pt, normal);
      auto proj_prev_pt = projectPointToPlane(prev_pt, pt, normal);

      fill_extended_path(proj_prev_pt, proj_next_pt, extended_path_exterior);
      fill_extended_path(proj_next_pt, proj_prev_pt, extended_path_interior);
#else // méthode raffin
      Vector3f prev_to_next            = next_pt - prev_pt;
      Vector3f prev_to_next_normalized = prev_to_next.normalized();

      auto fill_extended_path = [&](const Vector3f& normal, std::vector<Mesh::VertexHandle>& extended_path) {
        float minDist = std::numeric_limits<float>::max();
        Mesh::VertexHandle minVH;

        std::cout << "enter fill_extended_path" << std::endl;

        for (auto candidate : candidates) {
          if (candidate == prev_vh || candidate == next_vh) {
            std::cout << "pass prev or next" << std::endl;
            continue;
          }

          const auto& current_pt = mesh.point(candidate);

          if ((current_pt - pt).dot(normal) < 0) {
            std::cout << "pass dot" << std::endl;
            continue;
          }

          float dist = std::abs(pointToPlaneDistance(current_pt, pt, prev_to_next_normalized));
          std::cout << "dist : " << dist << std::endl;

          {
            auto d_vh1 = debug_mesh.new_vertex(current_pt);
            auto d_vh2 = debug_mesh.new_vertex(projectPointToPlane(current_pt, pt, prev_to_next_normalized));
            auto d_eh1 = debug_mesh.edge_handle(debug_mesh.new_edge(d_vh1, d_vh2));
            debug_mesh.set_color(d_eh1, Mesh::Color(255, 0, 0));
            debug_mesh.data(d_eh1).thickness = 10;
          }

          if (dist < minDist) {
            std::cout << "set dist" << std::endl;
            minDist = dist;
            minVH   = candidate;
          }
        }

        if (minVH.is_valid()) {
          std::cout << "minVH ok" << std::endl;
          extended_path.push_back(minVH);
          mesh.set_color(minVH, Mesh::Color(0, 85, 255));
          mesh.data(minVH).thickness = 16;
        } else {
          std::cout << "pas trouvé minVH" << std::endl;
        }
      };

      Vector3f meanNormalCandidates(0.f, 0.f, 0.f);
      for (auto candidate : candidates) {
        meanNormalCandidates += mesh.normal(candidate);
      }
      meanNormalCandidates.normalize();

      Vector3f exterior_normal =
          (mesh.normal(prev_vh) + mesh.normal(vh) + mesh.normal(next_vh) + meanNormalCandidates).cross(prev_to_next);
      Vector3f interior_normal = -exterior_normal;

      {
        auto d_vh1 = debug_mesh.new_vertex(pt);
        auto d_vh2 = debug_mesh.new_vertex(pt + exterior_normal.normalized() / 10.f);
        auto d_eh1 = debug_mesh.edge_handle(debug_mesh.new_edge(d_vh1, d_vh2));
        debug_mesh.set_color(d_eh1, Mesh::Color(100, 0, 200));
        debug_mesh.data(d_eh1).thickness = 10;
      }
      {
        auto d_vh1 = debug_mesh.new_vertex(pt);
        auto d_vh2 = debug_mesh.new_vertex(pt + interior_normal.normalized() / 10.f);
        auto d_eh1 = debug_mesh.edge_handle(debug_mesh.new_edge(d_vh1, d_vh2));
        debug_mesh.set_color(d_eh1, Mesh::Color(100, 0, 250));
        debug_mesh.data(d_eh1).thickness = 10;
      }

      fill_extended_path(exterior_normal, extended_path_exterior);
      fill_extended_path(interior_normal, extended_path_interior);
#endif
    }

    auto exterior_path = livewire(mesh, extended_path_exterior);
    auto interior_path = livewire(mesh, extended_path_interior);

    LivewireResult* livewire_result = nullptr;
    if (&mesh == &mesh1) {
      livewire_result = &LIVEWIRE_M1;
    } else if (&mesh == &mesh2) {
      livewire_result = &LIVEWIRE_M2;
    }

    if (livewire_result) {
      livewire_result->points          = std::move(protruding_vertices);
      livewire_result->result          = std::move(livewire_path);
      livewire_result->exterior_points = std::move(extended_path_exterior);
      livewire_result->exterior_result = std::move(exterior_path);
      livewire_result->interior_points = std::move(extended_path_interior);
      livewire_result->interior_result = std::move(interior_path);
    }
  };

  Mesh* mesh  = nullptr;
  DrawId* dId = nullptr;
  switch (ui->protruding_edges_mesh->value()) {
  case 1:
    if (!mesh1Loaded) {
      return;
    }
    mesh = &mesh1;
    dId  = &mesh1drawID;
    break;
  case 2:
    if (!mesh2Loaded) {
      return;
    }
    mesh = &mesh2;
    dId  = &mesh2drawID;
    break;
  default:
    return;
  }

  show_protruding_edges(*mesh);
  *dId = ui->displayWidget->displayMesh(mesh, DisplayMode::Normal, *dId);
  update_meshes();

  static DrawId meshDraw = 0;
  meshDraw               = ui->displayWidget->displayMesh(&debug_mesh, DisplayMode::Normal, meshDraw);
  ui->displayWidget->update();
}

void MainWindow::on_protruding_edges_export_clicked() {
  Mesh* mesh                      = nullptr;
  LivewireResult* livewire_result = nullptr;
  QString* filename               = nullptr;
  switch (ui->protruding_edges_mesh->value()) {
  case 1:
    if (!mesh1Loaded) {
      return;
    }
    mesh            = &mesh1;
    livewire_result = &LIVEWIRE_M1;
    filename        = &filename1;
    break;
  case 2:
    if (!mesh2Loaded) {
      return;
    }
    mesh            = &mesh2;
    livewire_result = &LIVEWIRE_M2;
    filename        = &filename2;
    break;
  default:
    return;
  }

  auto write = [&](const QString& fileName,
                   const std::vector<Mesh::VertexHandle>& points,
                   const std::vector<Mesh::VertexHandle>& livewire_path) {
    std::ofstream outfile(fileName.toStdString());
    outfile << std::showpoint << std::fixed << std::setprecision(std::numeric_limits<long double>::digits10 + 1);

    std::unordered_set<Mesh::VertexHandle> set_points(points.begin(), points.end());

    outfile << "; " << filename->toStdString() << '\n';
    outfile << "indice,est param livewire,x,y,z,angle dièdre moyen,courbure gaussienne, courbure moyenne, normale x, normale y, "
               "normale z\n";
    for (size_t i = 0; i < livewire_path.size(); ++i) {
      auto vh       = livewire_path[i];
      auto is_param = set_points.find(vh) != set_points.end();
      const auto& p = mesh->point(vh);
      auto prev_i = i > 0 ? i - 1 : livewire_path.size() - 1, next_i = (i + 1) % livewire_path.size();
      auto mean_dihedral_angle = (mesh->data(mesh->edge_handle(mesh->find_halfedge(vh, livewire_path[prev_i]))).dihedralAngle +
                                  mesh->data(mesh->edge_handle(mesh->find_halfedge(vh, livewire_path[next_i]))).dihedralAngle) /
                                 2.f;
      const auto& n = mesh->data(vh).normalizedNormal;
      outfile << i << ',' << (is_param ? 1 : 0) << ',' << p[0] << ',' << p[1] << ',' << p[2] << ',' << mean_dihedral_angle << ','
              << mesh->data(vh).kCurv << ',' << mesh->data(vh).hCurv << ',' << n[0] << ',' << n[1] << ',' << n[2] << '\n';
    }
  };

  auto fileName = QFileDialog::getSaveFileName(this, tr("Exporter livewire"), *filename, tr("CSV (*.csv)"));
  if (!fileName.isEmpty()) {
    write(fileName, livewire_result->points, livewire_result->result);
  }

  auto fileNameExterior = QFileDialog::getSaveFileName(this, tr("Exporter livewire extérieur"), *filename, tr("CSV (*.csv)"));
  if (!fileNameExterior.isEmpty()) {
    write(fileNameExterior, livewire_result->exterior_points, livewire_result->exterior_result);
  }

  auto fileNameInterior = QFileDialog::getSaveFileName(this, tr("Exporter livewire intérieur"), *filename, tr("CSV (*.csv)"));
  if (!fileNameInterior.isEmpty()) {
    write(fileNameInterior, livewire_result->interior_points, livewire_result->interior_result);
  }
}

void MainWindow::on_acp_compute_clicked() {
  static DrawId dId1 = 0, dId2 = 0;
  Mesh* mesh  = nullptr;
  DrawId* dId = nullptr;
  switch (ui->acp_mesh->value()) {
  case 1:
    if (!mesh1Loaded) {
      return;
    }
    mesh = &mesh1;
    dId  = &dId1;
    break;
  case 2:
    if (!mesh2Loaded) {
      return;
    }
    mesh = &mesh2;
    dId  = &dId2;
    break;
  default:
    return;
  }

  auto pca_result = pca(*mesh);
  std::cout << "pca eigenvalues: \n" << pca_result.eigenvalues << std::endl;
  std::cout << "pca eigenvectors: \n" << pca_result.eigenvectors << std::endl;

  Mesh pca_mesh;
  Vector3f mean{pca_result.mean[0], pca_result.mean[1], pca_result.mean[2]};
  auto meanVH = pca_mesh.new_vertex(mean);
  for (size_t i = 0; i < 3; ++i) {
    auto eh = pca_mesh.edge_handle(pca_mesh.new_edge(meanVH, pca_mesh.new_vertex(mean + pca_result.eigenvectors.col(i))));
    pca_mesh.set_color(eh, Mesh::Color(0, 127.f + i / 2.f * 127.f, 0));
    pca_mesh.data(eh).thickness = 6;
  }

  *dId = ui->displayWidget->displayMesh(&pca_mesh, DisplayMode::Normal, *dId);
  ui->displayWidget->update();
}

void MainWindow::on_pushButton_decimation_clicked() {
  if (mesh1Loaded) {
    decimation(mesh1);
    mesh1.update_face_normals();
    mesh1.update_vertex_normals();
    update_algo_properties(mesh1);
    mesh1.updateProperties();
    mesh1.resetAllColorsAndThickness();
    mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::Normal, mesh1drawID);
  }

  if (mesh2Loaded) {
    decimation(mesh2);
    mesh2.update_face_normals();
    mesh2.update_vertex_normals();
    update_algo_properties(mesh2);
    mesh2.updateProperties();
    mesh2.resetAllColorsAndThickness();
    mesh2drawID = ui->displayWidget->displayMesh(&mesh2, DisplayMode::Normal, mesh2drawID);
  }

  update_meshes();
}

void MainWindow::on_correspondences_compute_clicked() {
  if (!mesh1Loaded || !mesh2Loaded) {
    return;
  }

  auto getKeyPoits = [](const Mesh& mesh, std::vector<Mesh::VertexHandle>& candidates) {
    for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
      if (mesh.data(*v_it).isKeyPoint) {
        candidates.push_back(*v_it);
      }
    }
  };

  std::vector<Mesh::VertexHandle> keyPoints1, keyPoints2;
  if (LIVEWIRE_M1.result.empty() || LIVEWIRE_M2.result.empty()) {
    getKeyPoits(mesh1, keyPoints1);
    getKeyPoits(mesh2, keyPoints2);
  } else {
    keyPoints1 = LIVEWIRE_M1.result;
    keyPoints2 = LIVEWIRE_M2.result;
  }

  using GetProperty = std::function<float(const Mesh& mesh, Mesh::VertexHandle vh)>;
  auto threshold    = ui->correspondences_threshold->value();

  struct Correspondence {
    Mesh::VertexHandle v;
    std::vector<Mesh::VertexHandle> candidates;
  };

  std::vector<Correspondence> correspondences;
  for (auto v1 : keyPoints1) {
    Correspondence c;
    c.v          = v1;
    c.candidates = keyPoints2;
    correspondences.push_back(std::move(c));
  }

  auto filterByPropery = [&](const GetProperty& getProperty) {
    correspondences.erase(std::remove_if(correspondences.begin(),
                                         correspondences.end(),
                                         [&](Correspondence& c) {
                                           auto p1 = getProperty(mesh1, c.v);
                                           c.candidates.erase(
                                               std::remove_if(c.candidates.begin(),
                                                              c.candidates.end(),
                                                              [this, &getProperty, threshold, p1](Mesh::VertexHandle vh) {
                                                                auto p2 = getProperty(mesh2, vh);
                                                                return std::abs(p1 - p2) > threshold;
                                                              }),
                                               c.candidates.end());
                                           return c.candidates.empty();
                                         }),
                          correspondences.end());
  };

  auto get1RingProperty = [](const GetProperty& getProperty) {
    return [&getProperty](const Mesh& mesh, Mesh::VertexHandle vh) {
      float sum   = 0.f;
      float count = 0.f;
      for (auto vv_it = mesh.cvv_iter(vh); vv_it.is_valid(); ++vv_it) {
        sum += getProperty(mesh, *vv_it);
        count += 1;
      }
      return sum / count;
    };
  };
  auto get2RingProperty = [](const GetProperty& getProperty) {
    return [&getProperty](const Mesh& mesh, Mesh::VertexHandle vh) {
      std::vector<Mesh::VertexHandle> ring1;
      std::unordered_set<Mesh::VertexHandle> ring2;
      for (auto vv_it1 = mesh.cvv_iter(vh); vv_it1.is_valid(); ++vv_it1) {
        ring1.push_back(*vv_it1);
        for (auto vv_it2 = mesh.cvv_iter(*vv_it1); vv_it2.is_valid(); ++vv_it2) {
          ring2.insert(*vv_it2);
        }
      }
      ring2.erase(vh);
      for (auto r1vh : ring1) {
        ring2.erase(r1vh);
      }

      float sum   = 0.f;
      float count = 0.f;
      for (auto r2vh : ring2) {
        sum += getProperty(mesh, r2vh);
        count += 1;
      }
      return sum / count;
    };
  };

  GetProperty getProperty = [](const Mesh& mesh, Mesh::VertexHandle vh) { return mesh.data(vh).hCurv; };
  if (ui->correspondences_filter_vertex->isChecked()) {
    filterByPropery(getProperty);
  }
  if (ui->correspondences_filter_1ring->isChecked()) {
    filterByPropery(get1RingProperty(getProperty));
  }
  if (ui->correspondences_filter_2ring->isChecked()) {
    filterByPropery(get2RingProperty(getProperty));
  }

  Mesh mesh;
  for (const auto& c : correspondences) {
    auto vh1 = mesh.new_vertex(mesh1.point(c.v));
    for (const auto& c : c.candidates) {
      auto vh2 = mesh.new_vertex(mesh2.point(c));
      mesh.new_edge(vh1, vh2);
    }
  }
  mesh.resetAllColorsAndThickness();

  static DrawId meshDraw = 0;
  meshDraw               = ui->displayWidget->displayMesh(&mesh, DisplayMode::Normal, meshDraw);
  ui->displayWidget->update();
}

void MainWindow::on_export_prop_compute_clicked() {
  switch (ui->export_prop_mesh->value()) {
  case 1:
    if (!mesh1Loaded) {
      return;
    }
    break;
  case 2:
    if (!mesh2Loaded) {
      return;
    }
    break;
  default:
    return;
  }

  using GetProperty     = std::function<float(const Mesh& mesh, Mesh::VertexHandle vh)>;
  auto get1RingProperty = [](const GetProperty& getProperty) {
    return [&getProperty](const Mesh& mesh, Mesh::VertexHandle vh) {
      float sum   = 0.f;
      float count = 0.f;
      for (auto vv_it = mesh.cvv_iter(vh); vv_it.is_valid(); ++vv_it) {
        sum += getProperty(mesh, *vv_it);
        count += 1;
      }
      return sum / count;
    };
  };
  auto get2RingProperty = [](const GetProperty& getProperty) {
    return [&getProperty](const Mesh& mesh, Mesh::VertexHandle vh) {
      std::vector<Mesh::VertexHandle> ring1;
      std::unordered_set<Mesh::VertexHandle> ring2;
      for (auto vv_it1 = mesh.cvv_iter(vh); vv_it1.is_valid(); ++vv_it1) {
        ring1.push_back(*vv_it1);
        for (auto vv_it2 = mesh.cvv_iter(*vv_it1); vv_it2.is_valid(); ++vv_it2) {
          ring2.insert(*vv_it2);
        }
      }
      ring2.erase(vh);
      for (auto r1vh : ring1) {
        ring2.erase(r1vh);
      }

      float sum   = 0.f;
      float count = 0.f;
      for (auto r2vh : ring2) {
        sum += getProperty(mesh, r2vh);
        count += 1;
      }
      return sum / count;
    };
  };

  auto method   = ui->export_prop_property->currentText();
  auto fileName = QFileDialog::getSaveFileName(this, tr("Exporter propriétés"), method, tr("CSV (*.csv)"));
  if (fileName.isEmpty()) {
    return;
  }

  std::function<float(const Mesh&, Mesh::VertexHandle)> getProperty;
  if (method == "Courbure Moyenne") {
    getProperty = [](const Mesh& mesh, Mesh::VertexHandle vh) { return mesh.data(vh).hCurv; };
  } else if (method == "Courbure Gaussienne") {
    getProperty = [](const Mesh& mesh, Mesh::VertexHandle vh) { return mesh.data(vh).kCurv; };
  } else {
    return;
  }
  auto get1ring = get1RingProperty(getProperty);
  auto get2ring = get2RingProperty(getProperty);

  Mesh* mesh = nullptr;
  switch (ui->export_prop_mesh->value()) {
  case 1:
    mesh = &mesh1;
    break;
  case 2:
    mesh = &mesh2;
    break;
  default:
    return;
  }

  std::ofstream outfile(fileName.toStdString());
  outfile << std::showpoint << std::fixed << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
  for (auto v_it = mesh->vertices_begin(); v_it != mesh->vertices_end(); ++v_it) {
    if (ui->export_prop_keypts->isChecked() && !mesh->data(*v_it).isKeyPoint) {
      continue;
    }

    bool first = true;

    if (ui->export_prop_vertex->isChecked()) {
      if (first) {
        first = false;
      } else {
        outfile << ",";
      }
      outfile << getProperty(*mesh, *v_it);
    }
    if (ui->export_prop_1ring->isChecked()) {
      if (first) {
        first = false;
      } else {
        outfile << ",";
      }
      outfile << get1ring(*mesh, *v_it);
    }
    if (ui->export_prop_2ring->isChecked()) {
      if (first) {
        first = false;
      } else {
        outfile << ",";
      }
      outfile << get2ring(*mesh, *v_it);
    }
    outfile << "\n";
  }
}

void MainWindow::on_icp_compute_clicked() {
  if (!mesh1Loaded || !mesh2Loaded) {
    return;
  }

  ICPResult r;
  if (ui->icp_use_property->isChecked()) {
    std::function<float(const Mesh&, Mesh::VertexHandle)> getProperty;
    auto method = ui->icp_property->currentText();
    if (method == "Courbure Moyenne") {
      getProperty = [](const Mesh& mesh, Mesh::VertexHandle vh) { return mesh.data(vh).hCurv; };
    } else if (method == "Courbure Gaussienne") {
      getProperty = [](const Mesh& mesh, Mesh::VertexHandle vh) { return mesh.data(vh).kCurv; };
    } else {
      return;
    }

    r = icp(mesh1, mesh2, ui->icp_max_iterations->value(), getProperty, ui->icp_threshold->value());
  } else {
    r = icp(mesh1, mesh2, ui->icp_max_iterations->value());
  }
  lastIcpTransform = r.transformation;
  update_meshes();

  ui->icp_results->setText(QString("has converged: %1\nscore: %2").arg(r.hasConverged).arg(r.fitnessScore));
  qDebug() << "has converged:" << r.hasConverged << " score: " << r.fitnessScore;
}

void MainWindow::on_icp_show_transform_clicked() {
  update_meshes();
}

void MainWindow::on_icp_apply_transform_clicked() {
  Eigen::Affine3f t(lastIcpTransform);
  for (auto v_it = mesh1.vertices_begin(); v_it != mesh1.vertices_end(); ++v_it) {
    auto& v = mesh1.point(*v_it);
    v       = t * v;
  }

  mesh1drawID = ui->displayWidget->displayMesh(&mesh1, DisplayMode::Normal, mesh1drawID);
  lastIcpTransform.setIdentity();
  update_meshes();
}

void MainWindow::on_hausdorff_compute_clicked() {
  if (!mesh1Loaded || !mesh2Loaded) {
    return;
  }

  auto fillCloud = [&](const Mesh& mesh, Cloud& cloud) {
    if (ui->hausdorff_keypts->isChecked()) {
      cloud.resize(mesh.keyPoints().size());

      for (size_t i = 0; i < mesh.keyPoints().size(); ++i) {
        cloud.at(i).getVector3fMap() = mesh.point(mesh.keyPoints()[i]);
      }
    } else {
      cloud.resize(mesh.n_vertices());

      size_t cloudIndex = 0;
      for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
        cloud.at(cloudIndex).getVector3fMap() = mesh.point(*v_it);
        cloudIndex += 1;
      }
    }
  };

  Cloud c1, c2;
  fillCloud(mesh1, c1);
  fillCloud(mesh2, c2);

  auto r = hausdorff(c1, c2);
  ui->hausdorff_results->setText(QString("Distance : %1").arg(r));
  qDebug() << "Distance de hausdorff : " << r;
}

void MainWindow::update_meshes() {
  if (mesh1drawID > 0) {
    ui->displayWidget->isShown(mesh1drawID)        = ui->checkBox_isShown_1->isChecked();
    ui->displayWidget->alphaThreshold(mesh1drawID) = ui->checkBox_keyPts_1->isChecked() ? 0.1f : 0.f;
    copy(ui->displayWidget->matrix(mesh1drawID), ui->icp_show_transform->isChecked() ? lastIcpTransform : Matrix4x4f::Identity());
  }

  if (mesh2drawID > 0) {
    ui->displayWidget->isShown(mesh2drawID)        = ui->checkBox_isShown_2->isChecked();
    ui->displayWidget->alphaThreshold(mesh2drawID) = ui->checkBox_keyPts_2->isChecked() ? 0.1f : 0.f;
  }

  ui->displayWidget->update();
}
