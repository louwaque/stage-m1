#ifndef HAUSDORFF
#define HAUSDORFF

#include "types.h"

float hausdorff(const Cloud& cloud1, const Cloud& cloud2);

#endif