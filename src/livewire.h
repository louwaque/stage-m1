#ifndef LIVEWIRE_H_
#define LIVEWIRE_H_

#include "mesh.h"

// retourne la liste d'au plus edges_count sommets appartenant à des arêtes à forte saillance
// les sommets sont espacés d'une distance minimale edges_threshold
std::vector<Mesh::VertexHandle> get_protruding_vertices(Mesh& mesh, size_t edges_count, float edges_threshold);

// réordonne les sommets en fonction de leur angle autour de leur barycentre
void sort_by_angle(Mesh& mesh, std::vector<Mesh::VertexHandle>& vertices);

// retourne la liste des sommets définissant un polygone passant par tous les sommets de points
std::vector<Mesh::VertexHandle> livewire(Mesh& mesh, const std::vector<Mesh::VertexHandle>& points);

#endif
