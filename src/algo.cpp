#include "algo.h"

#include <cmath>

#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

using namespace OpenMesh;

#include <iostream>

float clamp(float v, float lo, float hi) {
  assert(!(hi < lo));
  return (v < lo) ? lo : (hi < v) ? hi : v;
}

float clampedAcos(float value) {
  return std::acos(clamp(value, -1.f, 1.f));
}

void update_algo_properties(Mesh& mesh) {
  auto faceArea = [&](FaceHandle face) {
    Mesh::Point vertices[3];
    mesh.faceVertices(face, vertices);

    return (vertices[0] - vertices[1]).cross(vertices[2] - vertices[1]).norm() / 2.f;
  };

  for (auto f_it = mesh.faces_begin(); f_it != mesh.faces_end(); f_it++) {
    auto& d            = mesh.data(*f_it);
    d.area             = faceArea(*f_it);
    d.normalizedNormal = mesh.normal(*f_it).normalized();
  }

  auto barycentricArea = [&](VertexHandle vertex) {
    float barycentricArea = 0;
    for (auto vf_it = mesh.vf_iter(vertex); vf_it.is_valid(); ++vf_it) {
      barycentricArea += mesh.data(*vf_it).area;
    }
    return barycentricArea / 3.f;
  };

  auto distance = [&](VertexHandle vertex0, VertexHandle vertex1) { return (mesh.point(vertex1) - mesh.point(vertex0)).norm(); };

  auto angleFF = [&](FaceHandle face0, FaceHandle face1, VertexHandle vert0, VertexHandle vert1) {
    auto n0    = mesh.data(face0).normalizedNormal;
    auto n1    = mesh.data(face1).normalizedNormal;
    auto arc   = mesh.point(vert1) - mesh.point(vert0);
    auto angle = clampedAcos(n0.dot(n1));

    if ((n0.cross(n1).dot(arc)) < 0) {
      angle = -angle;
    }

    return angle;
  };

  auto angleEE = [&](VertexHandle vertex, FaceHandle face) {
    Mesh::VertexHandle vertices[2];
    for (auto fh_it = mesh.fh_iter(face); fh_it.is_valid(); ++fh_it) {
      if (mesh.to_vertex_handle(*fh_it) == vertex) {
        vertices[0] = mesh.from_vertex_handle(*fh_it);
        vertices[1] = mesh.to_vertex_handle(mesh.next_halfedge_handle(*fh_it));
        break;
      }
    }

    const auto& P = mesh.point(vertex);
    return clampedAcos((mesh.point(vertices[0]) - P).normalized().dot((mesh.point(vertices[1]) - P).normalized()));
  };

  auto H_CurvVertex = [&](VertexHandle vertex) {
    float sum = 0;
    for (auto voh_it = mesh.voh_iter(vertex); voh_it.is_valid(); ++voh_it) {
      // TODO c grave ?
      if (mesh.is_boundary(mesh.edge_handle(*voh_it))) {
        continue;
      }
      auto opposedVertex = mesh.to_vertex_handle(*voh_it);
      auto angle         = angleFF(mesh.face_handle(*voh_it), mesh.opposite_face_handle(*voh_it), vertex, opposedVertex);
      sum += angle * distance(vertex, opposedVertex);
    }

    return sum / (4.f * mesh.data(vertex).barycentricArea);
  };

  auto K_CurvVertex = [&](VertexHandle vertex) {
    float tetaSum = 0;
    for (auto vf_it = mesh.vf_iter(vertex); vf_it.is_valid(); ++vf_it) {
      tetaSum += angleEE(vertex, *vf_it);
    }

    // https://computergraphics.stackexchange.com/questions/1718/what-is-the-simplest-way-to-compute-principal-curvature-for-a-mesh-triangle#comment3171_1721
    return ((mesh.is_boundary(vertex) ? 1.f : 2.f) * M_PI - tetaSum) / mesh.data(vertex).barycentricArea;
  };

  for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
    auto& d            = mesh.data(*v_it);
    d.normalizedNormal = mesh.normal(*v_it).normalized();
    d.barycentricArea  = barycentricArea(*v_it);
    d.hCurv            = H_CurvVertex(*v_it);
    d.kCurv            = K_CurvVertex(*v_it);
  }

  for (auto e_it = mesh.edges_begin(); e_it != mesh.edges_end(); ++e_it) {
    auto& d         = mesh.data(*e_it);
    d.dihedralAngle = mesh.calc_dihedral_angle(*e_it);
  }
}

void decimation(Mesh& mesh) {
  mesh.request_vertex_status();
  for (auto he_it = mesh.halfedges_begin(); he_it != mesh.halfedges_end(); ++he_it) {
    if (mesh.is_boundary(*he_it)) {
      mesh.status(mesh.to_vertex_handle(*he_it)).set_locked(true);
      mesh.status(mesh.from_vertex_handle(*he_it)).set_locked(true);
    }
  }

  Decimater::DecimaterT<Mesh> decimater(mesh);
  Decimater::ModQuadricT<Mesh>::Handle hMod;
  decimater.add(hMod); // register module at the decimater
  /*
   * since we need exactly one priority module (non-binary)
   * we have to call set_binary(false) for our priority module
   * in the case of HModQuadric, unset_max_err() calls set_binary(false)
   * internally
   */
  decimater.module(hMod).unset_max_err();
  decimater.initialize();
  decimater.decimate(mesh.n_edges() / 4);
  mesh.garbage_collection();
}

// from https://math.stackexchange.com/questions/544946/determine-if-projection-of-3d-point-onto-plane-is-within-a-triangle
bool pointInTriangle(const Vector3f& query_point,
                     const Vector3f& triangle_vertex_0,
                     const Vector3f& triangle_vertex_1,
                     const Vector3f& triangle_vertex_2) {
  // u=P2−P1
  auto u = triangle_vertex_1 - triangle_vertex_0;
  // v=P3−P1
  auto v = triangle_vertex_2 - triangle_vertex_0;
  // n=u×v
  auto n = u.cross(v);
  // w=P−P1
  auto w = query_point - triangle_vertex_0;
  // Barycentric coordinates of the projection P′of P onto T:
  // γ=[(u×w)⋅n]/n²
  auto gamma = u.cross(w).dot(n) / n.dot(n);
  // β=[(w×v)⋅n]/n²
  auto beta  = w.cross(v).dot(n) / n.dot(n);
  auto alpha = 1 - gamma - beta;
  // The point P′ lies inside T if:
  return (0 <= alpha) && (alpha <= 1) && (0 <= beta) && (beta <= 1) && (0 <= gamma) && (gamma <= 1);
}

float pointToPlaneDistance(const Vector3f& point, const Vector3f& planePoint, const Vector3f& planeNormal) {
  return (point - planePoint).dot(planeNormal);
}

Vector3f projectPointToPlane(const Vector3f& point, const Vector3f& planePoint, const Vector3f& planeNormal) {
  return point - pointToPlaneDistance(point, planePoint, planeNormal) * planeNormal;
}

float angle(const Vector3f& v1, const Vector3f& v2) {
  return clampedAcos(v1.normalized().dot(v2.normalized()));
}

float signedAngle(const Vector3f& v1, const Vector3f& v2, const Vector3f& normal) {
  return std::atan2(v1.cross(v2).dot(normal), v1.dot(v2));
}

void update_distance_aux_faces_v1(Mesh& mesh1, Mesh& mesh2) {
  auto update_distance_aux_faces = [](Mesh& mesh1, Mesh& mesh2) {
    for (auto vh : mesh1.keyPoints()) {
      const auto& p = mesh1.point(vh);
      Mesh::Point vertices[3];
      float distance_aux_faces = std::numeric_limits<float>::max();
      size_t count             = 0;
      for (auto f_it = mesh2.faces_begin(); f_it != mesh2.faces_end(); ++f_it) {
        mesh2.faceVertices(*f_it, vertices);

        if (!pointInTriangle(p, vertices[0], vertices[1], vertices[2])) {
          continue;
        }

        count += 1;
        distance_aux_faces = std::min(distance_aux_faces, pointToPlaneDistance(p, vertices[0], mesh2.normal(*f_it).normalized()));
      }
      mesh1.data(vh).distance_aux_faces = count > 0 ? distance_aux_faces : NAN;
    }
  };
  update_distance_aux_faces(mesh1, mesh2);
  update_distance_aux_faces(mesh2, mesh1);
}

void update_distance_aux_faces_v2(Mesh& mesh1, Mesh& mesh2) {
  auto update_distance_aux_faces = [](Mesh& mesh1, const Mesh& mesh2) {
    for (auto vh1 : mesh1.keyPoints()) {
      const auto& p = mesh1.point(vh1);
      float minDist = std::numeric_limits<float>::max();
      Mesh::VertexHandle minVert;
      for (auto vh2 : mesh2.keyPoints()) {
        float diff = (mesh2.point(vh2) - p).squaredNorm();
        if (diff < minDist) {
          minDist = diff;
          minVert = vh2;
        }
      }
      mesh1.data(vh1).distance_aux_faces = pointToPlaneDistance(p, mesh2.point(minVert), mesh2.data(minVert).normalizedNormal);
    }
  };
  update_distance_aux_faces(mesh1, mesh2);
  update_distance_aux_faces(mesh2, mesh1);
}