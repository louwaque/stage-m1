#include "mesh.h"

#include "algo.h"

#include <set>

void Mesh::resetAllColorsAndThickness() {
  for (auto curVert = vertices_begin(); curVert != vertices_end(); curVert++) {
    data(*curVert).thickness = 1;
    set_color(*curVert, Color(0, 0, 0));
  }

  for (auto curFace = faces_begin(); curFace != faces_end(); curFace++) {
    set_color(*curFace, Color(150, 150, 150));
  }

  for (auto curEdge = edges_begin(); curEdge != edges_end(); curEdge++) {
    data(*curEdge).thickness = 1;
    set_color(*curEdge, Color(0, 0, 0));
  }
}

void Mesh::faceVertices(FaceHandle face, Mesh::Point vertices[3]) const {
  auto fv_it = cfv_ccwiter(face);
  for (size_t i = 0; i < 3; ++i) {
    assert(fv_it.is_valid());
    vertices[i] = point(*fv_it);
    ++fv_it;
  }
}

void Mesh::nRingVertices(VertexHandle vh, size_t n, std::vector<Mesh::VertexHandle>& ring) const {
  std::set<Mesh::VertexHandle> visited;
  std::vector<VertexHandle> to_visit = {vh};
  std::vector<VertexHandle> next_to_visit;

  for (size_t r = n; r > 0; --r) {
    visited.insert(to_visit.begin(), to_visit.end());
    for (auto vh : to_visit) {
      for (auto vv_iter = cvv_ccwiter(vh); vv_iter.is_valid(); ++vv_iter) {
        if (visited.find(*vv_iter) != visited.end()) {
          continue;
        }
        next_to_visit.push_back(*vv_iter);
      }
    }
    swap(to_visit, next_to_visit);
    next_to_visit.clear();
  }

  ring = to_visit;
}

void Mesh::updateProperties() {
  // update bounding box
  _boundingBoxMin = Vector3f::Constant(std::numeric_limits<float>::max());
  _boundingBoxMax = Vector3f::Constant(std::numeric_limits<float>::min());

  for (auto v_it = vertices_begin(); v_it != vertices_end(); ++v_it) {
    const auto& pt  = point(*v_it);
    _boundingBoxMin = _boundingBoxMin.cwiseMin(pt);
    _boundingBoxMax = _boundingBoxMax.cwiseMax(pt);
  }

  // update key points
  _keyPoints.clear();
  for (auto v_it = vertices_begin(); v_it != vertices_end(); ++v_it) {
    if (data(*v_it).isKeyPoint) {
      _keyPoints.push_back(*v_it);
    }
  }

  // update key faces
  _keyFaces.clear();
  for (auto f_it = faces_begin(); f_it != faces_end(); ++f_it) {
    bool isKeyPoint = true;
    for (auto fv_it = fv_iter(*f_it); fv_it.is_valid() && isKeyPoint; ++fv_it) {
      isKeyPoint = data(*fv_it).isKeyPoint;
    }
    if (isKeyPoint) {
      _keyFaces.push_back(*f_it);
    }
  }

  // update key points normal
  _keyPointsNormal.fill(0.f);
  for (auto vh : keyPoints()) {
    _keyPointsNormal += normal(vh);
  }
  _keyPointsNormal.normalize();
}

bool Mesh::chargement(const std::string& fileName) {
  if (fileName.empty()) {
    return false;
  }

  using OpenMesh::IO::Options;

  Options opt;
  opt += Options::FaceNormal;
  opt += Options::VertexNormal;
  opt += Options::VertexColor;
  if (!OpenMesh::IO::read_mesh(*this, fileName, opt)) {
    return false;
  }

  if (!opt.face_has_normal()) {
    qWarning() << "Calcul des normales aux faces.";
    update_face_normals();
  }

  if (!opt.vertex_has_normal()) {
    qWarning() << "Calcul des normales aux sommets.";
    update_vertex_normals();
  }

  update_algo_properties(*this);

  if (opt.vertex_has_color()) {
    for (auto v_it = vertices_begin(); v_it != vertices_end(); ++v_it) {
      auto& d      = data(*v_it);
      d.isKeyPoint = color(*v_it) == Color(255, 255, 0);
      d.alpha      = d.isKeyPoint ? 255 : 0;
    }
  } else {
    qCritical() << "Le fichier OBJ n'a pas de couleurs aux sommets.";
  }

  resetAllColorsAndThickness();
  updateProperties();

  return true;
}
