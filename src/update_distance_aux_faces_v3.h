#ifndef UPDATE_DISTANCE_AUX_FACES_V3_H
#define UPDATE_DISTANCE_AUX_FACES_V3_H

#include "mesh.h"

// distance aux faces avec lancé de rayon
// angleThreshold en radians
void update_distance_aux_faces_v3(Mesh& mesh1, Mesh& mesh2, bool onlyKeyFaces, bool useRay, float angleThreshold);

#endif