#ifndef MESHVIEWERWIDGET_H
#define MESHVIEWERWIDGET_H

#define GL_GLEXT_PROTOTYPES
#include <QDebug>
#include <QGLWidget>
#include <QMouseEvent>
#include <QtOpenGL>

#include "mesh.h"

const double TRACKBALL_RADIUS = 0.6;
const bool OPENGL_DEBUG       = false;

using DrawId = size_t;
enum DisplayMode { Normal, TemperatureMap, ColorShading };

class MeshViewerWidget
: public QOpenGLWidget
, protected QOpenGLFunctions {
  Q_OBJECT

  struct Draw {
    QMatrix4x4 matrix;
    bool isShown         = true;
    float alphaThreshold = 0.f;

    QOpenGLBuffer TriDataBuffers[2];
    GLsizei triToDraw                = 0;
    QOpenGLVertexArrayObject* triVao = nullptr;

    QOpenGLBuffer LinesDataBuffers[2];
    GLsizei linesToDraw                = 0;
    QOpenGLVertexArrayObject* linesVao = nullptr;
    QList<QPair<float, int>> edgeSizes;

    QOpenGLBuffer PointsDataBuffers[2];
    GLsizei pointsToDraw                = 0;
    QOpenGLVertexArrayObject* pointsVao = nullptr;
    QList<QPair<float, int>> vertsSizes;

    void tearTriGLObjects();
    void tearLinesGLObjects();
    void tearPointsGLObjects();
    void tearGLObjects();
  };

public:
  explicit MeshViewerWidget(QWidget* _parent = nullptr);
  ~MeshViewerWidget() override;

  // ajoute un maillage à afficher
  // si id > 0 alors il sera l'identifiant de l'objet, l'objet précédement affiché est remplacé
  // retourne l'ide de l'objet
  DrawId displayMesh(Mesh* _mesh, DisplayMode mode = DisplayMode::Normal, DrawId id = 0);

  // retourne la matrice de transformation de l'objet
  QMatrix4x4& matrix(DrawId id);
  const QMatrix4x4& matrix(DrawId id) const;

  // retourne si l'objet doit être affiché ou non
  bool& isShown(DrawId id);
  bool isShown(DrawId id) const;

  // retourne le seuil de l'alpha
  // les sommets on une valeur alpha et si elle est inférieure à ce seuil alors ils ne sont pas affichés
  float& alphaThreshold(DrawId id);
  float alphaThreshold(DrawId id) const;

  // supprime l'objet id
  void remove(DrawId id);

  // supprime tous les objets
  void clear();

public slots:
  void messageLogged(QOpenGLDebugMessage message);

protected:
  void initializeGL() override;
  void resizeGL(int w, int h) override;
  void paintGL() override;
  void mousePressEvent(QMouseEvent* ev) override;
  void mouseReleaseEvent(QMouseEvent* ev) override;
  void mouseMoveEvent(QMouseEvent* ev) override;
  void wheelEvent(QWheelEvent* ev) override;

  // gestion de la vue et de la trackball
  void update_projection_matrix();
  void view_all();
  void set_scene_pos(const QVector3D& _cog, float _radius);
  bool map_to_sphere(const QPoint& _point, QVector3D& _result);
  void translate(const QVector3D& _trans);
  void rotate(const QVector3D& _axis, float _angle);

  // TODO toujours besoin ?
  Draw& getOrNewDraw(DrawId& id, const Draw& defaultDraw);

  void loadMesh(Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* triangles, int nTriangles);
  void loadLines(Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* lines, int nLines, QList<QPair<float, int>> es);
  void loadPoints(
      Draw& draw, GLfloat* verts, GLfloat* colors, int nVerts, GLuint* points, int nPoints, QList<QPair<float, int>> vs);

private:
  float m_radius = 0.5f;
  QVector3D m_center;

  QMatrix4x4 m_modelView, m_projection;

  QPoint last_point_2D_;
  QVector3D last_point_3D_;
  bool last_point_ok_;

  QOpenGLShaderProgram* m_program;

  DrawId next_id = 1;
  std::map<DrawId, Draw> m_draws;
};

#endif // MESHVIEWERWIDGET_H
