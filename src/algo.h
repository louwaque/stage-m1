#ifndef ALGO_H
#define ALGO_H

#include "mesh.h"

// retourne acos sur une valeur qui est recoupé entre [-1 , 1]
float clampedAcos(float value);

// met à jour les propriétés du maillage
// les normales aux faces et sommets doivent être déjà définies
void update_algo_properties(Mesh& mesh);

// décime le maillage selon "Quadratic Error Metric"
// le nombre d'arêtes divisé par 4
void decimation(Mesh& mesh);

// retourne vrai si query_point est dans le triangle (triangle_vertex_0, triangle_vertex_1, triangle_vertex_2)
bool pointInTriangle(const Vector3f& query_point,
                     const Vector3f& triangle_vertex_0,
                     const Vector3f& triangle_vertex_1,
                     const Vector3f& triangle_vertex_2);

// retourne la distance entre point et la surface passant par planePoint de normale planeNormal
float pointToPlaneDistance(const Vector3f& point, const Vector3f& planePoint, const Vector3f& planeNormal);

// retourne le projetté du point sur la face passant par planePoint de normale planeNormal
Vector3f projectPointToPlane(const Vector3f& point, const Vector3f& planePoint, const Vector3f& planeNormal);

// retourne un angle entre [0 , π]
float angle(const Vector3f& v1, const Vector3f& v2);

// normal doit être normalisé
// retourne un angle entre [-π , +π]
float signedAngle(const Vector3f& v1, const Vector3f& v2, const Vector3f& normal);

// algo avec test de projection
void update_distance_aux_faces_v1(Mesh& mesh1, Mesh& mesh2);

// algo avec normale au sommet comme face
void update_distance_aux_faces_v2(Mesh& mesh1, Mesh& mesh2);

#include "update_distance_aux_faces_v3.h"

#endif