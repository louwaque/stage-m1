#include "icp.h"

#include <pcl/point_types.h>
#include <pcl/registration/icp.h>

#include "hausdorff.h"
#include <iostream>

ICPResult icp(const Mesh& sourceMesh, const Mesh& targetMesh, int maximumIterations) {
  auto fillCloud = [&](const Mesh& mesh, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud) {
    size_t nbKeyPoints = mesh.keyPoints().size();

    cloud.reset(new pcl::PointCloud<pcl::PointXYZ>(nbKeyPoints, 1));

    for (size_t i = 0; i < nbKeyPoints; ++i) {
      cloud->at(i).getVector3fMap() = mesh.point(mesh.keyPoints()[i]);
    }
  };

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudSource, cloudTarget;
  fillCloud(sourceMesh, cloudSource);
  fillCloud(targetMesh, cloudTarget);

  pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  icp.setInputSource(cloudSource);
  icp.setInputTarget(cloudTarget);
  icp.setMaximumIterations(maximumIterations);

  pcl::PointCloud<pcl::PointXYZ> Final;
  icp.align(Final);

  ICPResult r;
  r.hasConverged   = icp.hasConverged();
  r.fitnessScore   = icp.getFitnessScore();
  r.transformation = icp.getFinalTransformation();
  return r;
}

class MyCR : public pcl::registration::CorrespondenceRejector {
public:
  using Ptr      = pcl::shared_ptr<MyCR>;
  using ConstPtr = pcl::shared_ptr<const MyCR>;

  MyCR() {}

  void setDistanceThreshold(float thresh) { this->thresh = thresh; }

  void setSourceFeature(const pcl::PointCloud<float>::ConstPtr& featureSource) { this->featureSource = featureSource; }

  void setTargetFeature(const pcl::PointCloud<float>::ConstPtr& featureTarget) { this->featureTarget = featureTarget; }

  void getRemainingCorrespondences(const pcl::Correspondences& original_correspondences,
                                   pcl::Correspondences& remaining_correspondences) override {
    assert(original_correspondences.size() == remaining_correspondences.size());
    remaining_correspondences.erase(std::remove_if(remaining_correspondences.begin(),
                                                   remaining_correspondences.end(),
                                                   [this](const pcl::Correspondence& c) {
                                                     return std::abs(featureSource->at(c.index_query) -
                                                                     featureTarget->at(c.index_match)) > thresh;
                                                   }),
                                    remaining_correspondences.end());
    // std::cout << "origin size: " << original_correspondences.size() << " remaining size: " << remaining_correspondences.size()
    //           << std::endl;
  }

protected:
  void applyRejection(pcl::Correspondences& correspondences) override {
    getRemainingCorrespondences(*input_correspondences_, correspondences);
  }

private:
  float thresh = std::numeric_limits<float>::max();
  pcl::PointCloud<float>::ConstPtr featureSource, featureTarget;
};

ICPResult icp(const Mesh& sourceMesh,
              const Mesh& targetMesh,
              int maximumIterations,
              std::function<float(const Mesh&, Mesh::VertexHandle)> getProperty,
              float propertyThreshold) {
  auto fillCloud = [&](const Mesh& mesh, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<float>::Ptr& feature) {
    size_t nbKeyPoints = std::count_if(
        mesh.vertices_begin(), mesh.vertices_end(), [&mesh](Mesh::VertexHandle vh) { return mesh.data(vh).isKeyPoint; });

    cloud.reset(new pcl::PointCloud<pcl::PointXYZ>(nbKeyPoints, 1));
    feature.reset(new pcl::PointCloud<float>(nbKeyPoints, 1));

    size_t cloudIndex = 0;
    for (auto v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
      if (!mesh.data(*v_it).isKeyPoint) {
        continue;
      }
      cloud->at(cloudIndex).getVector3fMap() = mesh.point(*v_it);
      feature->at(cloudIndex)                = getProperty(mesh, *v_it);
      cloudIndex += 1;
    }
  };

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudSource, cloudTarget;

  pcl::PointCloud<float>::Ptr featureSource, featureTarget;
  fillCloud(sourceMesh, cloudSource, featureSource);
  fillCloud(targetMesh, cloudTarget, featureTarget);

  MyCR::Ptr cr(new MyCR);
  cr->setDistanceThreshold(propertyThreshold);
  cr->setSourceFeature(featureSource);
  cr->setTargetFeature(featureTarget);

  pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
  icp.setInputSource(cloudSource);
  icp.setInputTarget(cloudTarget);
  icp.addCorrespondenceRejector(cr);
  icp.setMaximumIterations(maximumIterations);

  pcl::PointCloud<pcl::PointXYZ> Final;
  icp.align(Final);

  ICPResult r;
  r.hasConverged   = icp.hasConverged();
  r.fitnessScore   = icp.getFitnessScore();
  r.transformation = icp.getFinalTransformation();
  return r;
}